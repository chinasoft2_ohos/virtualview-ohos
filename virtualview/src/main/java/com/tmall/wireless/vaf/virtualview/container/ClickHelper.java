/*
 * MIT License
 *
 * Copyright (c) 2018 Alibaba Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.tmall.wireless.vaf.virtualview.container;

import ohos.agp.components.Component;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.multimodalinput.event.TouchEvent;
import com.tmall.wireless.vaf.framework.VafContext;
import com.tmall.wireless.vaf.virtualview.core.IContainer;
import com.tmall.wireless.vaf.virtualview.core.ViewBase;

/**
 * Created by gujicheng on 16/9/7.
 */
public class ClickHelper {
    private final static String TAG = "ClickHelper_TMTEST";

    //TODO 添加
    EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());

    protected final static int LONG_PRESS_THRESHOLD = 500;

    protected boolean mClickFinished = true;

    protected boolean mLongClickPressed = false;

    protected int mLastX;

    protected int mLastY;

    protected int mStartX;

    protected int mStartY;

    protected int mEndx;

    protected int mEndY;

    protected IContainer mContainer;

    protected LongRunnable mRunnable;

    //TODO make it static
    public ClickHelper(IContainer c) {
        mContainer = c;
        mRunnable = new LongRunnable();
        final Component holderView = c.getHolderView();
        final ViewBase vb = c.getVirtualView();
        holderView.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent motionEvent) {
                boolean ret = false;
                int action = motionEvent.getAction();
                switch (action) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        mClickFinished = false;
                        mLongClickPressed = false;
                        mStartX = (int) motionEvent.getPointerPosition(0).getX();
                        mStartY = (int) motionEvent.getPointerPosition(0).getY();
                        mLastX = mStartX;
                        mLastY = mStartY;
                        if (vb.handleEvent(mStartX, mStartY)) {
                            ret = true;

                            eventHandler.removeTask(mRunnable);
                            mRunnable.setView(mContainer.getVirtualView());
                            mRunnable.setHolderView(holderView);
                            eventHandler.sendEvent(InnerEvent.get(mRunnable), LONG_PRESS_THRESHOLD);
                            vb.onTouch(component, motionEvent);
                        } else {
                            ret = false;
                        }
                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                        final ViewBase vView = mContainer.getVirtualView();
                        if (null != vView && !mLongClickPressed) {
                            mEndx = (int) motionEvent.getPointerPosition(0).getX();
                            mEndY = (int) motionEvent.getPointerPosition(0).getY();
                            if (Math.abs(mEndx - mStartX) < 10 || Math.abs(mLastY - mStartY) < 10) {
                                ret = vView.click(mStartX, mStartY, false);
                            }
                            if (ret) {
                                //TODO 触摸声音反馈  暂无替代
//                                holderView.playSoundEffect(SoundEffectConstants.CLICK);
                            }
                        }
                        vb.onTouch(component, motionEvent);
                        mClickFinished = true;
                        break;
//
                    case TouchEvent.POINT_MOVE:
                        int currentX = (int) motionEvent.getPointerPosition(0).getX();
                        int currentY = (int) motionEvent.getPointerPosition(0).getY();
                        if (Math.sqrt(Math.pow(currentX - mLastX, 2) + Math.pow(currentY - mLastY, 2)) > VafContext.SLOP) {
//                            holderView.removeCallbacks(mRunnable);//移除之前add的延迟消息
                            eventHandler.removeTask(mRunnable);//TODO 暂时做修改 效果未知
                        }
                        mLastX = currentX;
                        mLastY = currentY;
                        vb.onTouch(component, motionEvent);
                        break;
                    case TouchEvent.CANCEL:
                        vb.onTouch(component, motionEvent);
                        mClickFinished = true;
                        break;
                }
                return ret;
            }
        });
//        holderView.setTouchEventListener(new View.OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                boolean ret = false;
//                int action = motionEvent.getAction();
//                switch (action) {
//                    case MotionEvent.ACTION_DOWN:
//                        mClickFinished = false;
//                        mLongClickPressed = false;
//                        mStartX = (int)motionEvent.getX();
//                        mStartY = (int)motionEvent.getY();
//                        mLastX = mStartX;
//                        mLastY = mStartY;
//                        if (vb.handleEvent(mStartX, mStartY)) {
//                            ret = true;
//                            Handler h = holderView.getHandler();
//                            h.removeCallbacks(mRunnable);
//                            mRunnable.setView(mContainer.getVirtualView());
//                            mRunnable.setHolderView(holderView);
//                            h.postDelayed(mRunnable, LONG_PRESS_THRESHOLD);
//                            vb.onTouch(view, motionEvent);
//                        } else {
//                            ret = false;
//                        }
//                        break;
//
//                    case MotionEvent.ACTION_UP:
//                        final ViewBase vView = mContainer.getVirtualView();
//                        if (null != vView && !mLongClickPressed) {
//                            ret = vView.click(mStartX, mStartY, false);
//                            if (ret) {
//                                holderView.playSoundEffect(SoundEffectConstants.CLICK);
//
//                            }
//                        }
//                        vb.onTouch(view, motionEvent);
//                        mClickFinished = true;
//                        break;
//
//                    case MotionEvent.ACTION_MOVE:
//                        int currentX = (int)motionEvent.getX();
//                        int currentY = (int)motionEvent.getY();
//                        if (Math.sqrt(Math.pow(currentX - mLastX, 2) + Math.pow(currentY - mLastY, 2))
//                            > VafContext.SLOP) {
//                            holderView.removeCallbacks(mRunnable);
//                        }
//                        mLastX = currentX;
//                        mLastY = currentY;
//                        vb.onTouch(view, motionEvent);
//                        break;
//
//                    case MotionEvent.ACTION_CANCEL:
//                        vb.onTouch(view, motionEvent);
//                        mClickFinished = true;
//                        break;
//                    default:
//                        break;
//                }
//
//                return ret;
//            }
//        });
    }

    class LongRunnable implements Runnable {
        protected ViewBase mView;

        protected Component mHolderView;

        public void setHolderView(Component holderView) {
            mHolderView = holderView;
        }

        public void setView(ViewBase v) {
            mView = v;
        }

        @Override
        public void run() {
            if (!mClickFinished && null != mView) {
                boolean ret = mView.click(mStartX, mStartY, true);
                if (ret && mHolderView != null) {
                    mLongClickPressed = true;
                    //TODO 长按后反馈
//                    mHolderView.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                }
            }
        }
    }
}
