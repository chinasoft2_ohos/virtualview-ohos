/*
 * MIT License
 *
 * Copyright (c) 2018 Alibaba Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.tmall.wireless.vaf.virtualview.Helper;

import com.libra.Color;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;

/**
 * Created by longerian on 2017/12/18.
 *
 * @author longerian
 * @date 2017/12/18
 */

public class VirtualViewUtils {

    private static boolean enableBorderRadius = true;

    public static void setEnableBorderRadius(boolean enableBorderRadius) {
        VirtualViewUtils.enableBorderRadius = enableBorderRadius;
    }

    private static RectFloat oval = new RectFloat();
    private static Path ovalPath = new Path();

    private static Path sPath = new Path();


    private static class BorderPaint {
        static Paint Instance = new Paint();
    }

    public static Paint getSBorderPaint() {
        return BorderPaint.Instance;
    }


    private static Paint sBackgroundPaint;

    public static void drawBorder(Canvas canvas, int borderColor, int width, int height, int borderWidth,
                                  int borderTopLeftRadius, int borderTopRightRadius, int borderBottomLeftRadius, int borderBottomRightRadius) {
        if (canvas == null || borderWidth <= 0 || borderColor == Color.TRANSPARENT) {
            return;
        }
        getSBorderPaint().setAntiAlias(true);
        getSBorderPaint().setStyle(Paint.Style.STROKE_STYLE);
//        sBorderPaint.setColor(borderColor);//TODO 修改
        getSBorderPaint().setColor(new ohos.agp.utils.Color(borderColor));

        getSBorderPaint().setStrokeWidth(borderWidth);
        if (!enableBorderRadius) {
            borderTopLeftRadius = 0;
            borderTopRightRadius = 0;
            borderBottomLeftRadius = 0;
            borderBottomRightRadius = 0;
        }
        float halfBorderWidth = (float) (borderWidth / (double) 2);
//        //draw left border
//        canvas.drawLine(halfBorderWidth, borderTopLeftRadius > 0 ? borderTopLeftRadius + halfBorderWidth : 0, halfBorderWidth,
//            borderBottomLeftRadius > 0 ? height - borderBottomLeftRadius - halfBorderWidth : height, sBorderPaint);
//
//        //draw top border
//        canvas.drawLine(borderTopLeftRadius > 0 ? halfBorderWidth + borderTopLeftRadius : 0, halfBorderWidth,
//            borderTopRightRadius > 0 ? width - borderTopRightRadius - halfBorderWidth : width, halfBorderWidth, sBorderPaint);
//
//        //draw right border
//        canvas.drawLine(width - halfBorderWidth, borderTopRightRadius > 0 ? halfBorderWidth + borderTopRightRadius : 0, width - halfBorderWidth,
//            borderBottomRightRadius > 0 ? height - borderBottomRightRadius - halfBorderWidth : height, sBorderPaint);
//
//        //draw bottom border
//        canvas.drawLine(borderBottomLeftRadius > 0 ? halfBorderWidth + borderBottomLeftRadius : 0, height - halfBorderWidth,
//            borderBottomRightRadius > 0 ? width - borderBottomRightRadius - halfBorderWidth : width, height - halfBorderWidth, sBorderPaint);
        //TODO 修改
        //draw left border

        //        //draw left border
        canvas.drawLine(new Point(halfBorderWidth, borderTopLeftRadius > 0 ? (float) (borderTopLeftRadius + (double) halfBorderWidth) : 0), new Point(halfBorderWidth,
                borderBottomLeftRadius > 0 ? (float) (height - borderBottomLeftRadius - (double) halfBorderWidth) : height), getSBorderPaint());
        //        //draw top border
        canvas.drawLine(new Point(borderTopLeftRadius > 0 ? (float) ((double) halfBorderWidth + borderTopLeftRadius) : 0, halfBorderWidth), new Point(
                borderTopRightRadius > 0 ? (float) (width - borderTopRightRadius - (double) halfBorderWidth) : width, halfBorderWidth), getSBorderPaint());
        //        //draw right border
        canvas.drawLine(new Point((float) (width - (double) halfBorderWidth), borderTopRightRadius > 0 ? (float) ((double) halfBorderWidth + borderTopRightRadius) : 0), new Point((float) (width - (double) halfBorderWidth),
                borderBottomRightRadius > 0 ? (float) (height - borderBottomRightRadius - (double) halfBorderWidth) : height), getSBorderPaint());
        //draw bottom border
        canvas.drawLine(new Point(borderBottomLeftRadius > 0 ? (float) ((double) halfBorderWidth + borderBottomLeftRadius) : 0, (float) (height - (double) halfBorderWidth)),
                new Point(borderBottomRightRadius > 0 ? (float) (width - borderBottomRightRadius - (double) halfBorderWidth) : width, (float) (height - (double) halfBorderWidth)), getSBorderPaint());

        //draw top left corner
        if (borderTopLeftRadius > 0) {
//            oval.set(0, 0, 2 * borderTopLeftRadius, 2 * borderTopLeftRadius);
//            oval.offset(halfBorderWidth, halfBorderWidth);
            //   canvas.drawArc(oval, new Arc(179, 91, false), sBorderPaint);
            oval.clear();
            ovalPath.reset();
            oval.fuse(0, 0, 2 * borderTopLeftRadius, 2 * borderTopLeftRadius);
            ovalPath.addArc(oval, 179, 91);
            ovalPath.offset(halfBorderWidth, halfBorderWidth);
            canvas.drawPath(ovalPath, getSBorderPaint());
        }

        //draw top right corner
        if (borderTopRightRadius > 0) {
//            oval.set(width - 2 * borderTopRightRadius, 0, width, 2 * borderTopRightRadius);
//            oval.offset(-halfBorderWidth, halfBorderWidth);
//            canvas.drawArc(oval, 269, 91, false, sBorderPaint);
            oval.clear();
            ovalPath.reset();
            oval.fuse(width - 2 * borderTopRightRadius, 0, width, 2 * borderTopRightRadius);
            ovalPath.addArc(oval, 269, 91);
            ovalPath.offset(-halfBorderWidth, halfBorderWidth);
            canvas.drawPath(ovalPath, getSBorderPaint());

        }

        //draw bottom right corner
        if (borderBottomRightRadius > 0) {
//            oval.set(width - 2 * borderBottomRightRadius, height - 2 * borderBottomRightRadius, width, height);
//            oval.offset(-halfBorderWidth, -halfBorderWidth);
//            canvas.drawArc(oval, -1, 91, false, sBorderPaint);
            oval.clear();
            ovalPath.reset();
            oval.fuse(width - 2 * borderBottomRightRadius, height - 2 * borderBottomRightRadius, width, height);
            ovalPath.addArc(oval, -1, 91);
            ovalPath.offset(-halfBorderWidth, -halfBorderWidth);
            canvas.drawPath(ovalPath, getSBorderPaint());
        }

        //draw bottom left corner
        if (borderBottomLeftRadius > 0) {
//            oval.set(0, height - 2 * borderBottomLeftRadius, 2 * borderBottomLeftRadius, height);
//            oval.offset(halfBorderWidth, -halfBorderWidth);
//            canvas.drawArc(oval, 89, 91, false, sBorderPaint);
            oval.clear();
            ovalPath.reset();
            oval.fuse(0, height - 2 * borderBottomLeftRadius, 2 * borderBottomLeftRadius, height);
            ovalPath.addArc(oval, 89, 91);
            ovalPath.offset(halfBorderWidth, -halfBorderWidth);
            canvas.drawPath(ovalPath, getSBorderPaint());

        }
    }

    public static void drawBackground(Canvas canvas, int backgruondColor, int width, int height, int borderWidth,
                                      int borderTopLeftRadius, int borderTopRightRadius, int borderBottomLeftRadius, int borderBottomRightRadius) {
        //  Log.i("hh", "drawBackground width " + width + " height " + height);
        if (canvas == null) {
            return;
        }
        if (null == sBackgroundPaint) {
            sBackgroundPaint = new Paint();
            sBackgroundPaint.setAntiAlias(true);
        }

//        sBackgroundPaint.setColor(backgruondColor);//TODO 修改
        sBackgroundPaint.setColor(new ohos.agp.utils.Color(backgruondColor));
        if (!enableBorderRadius) {
            borderTopLeftRadius = 0;
            borderTopRightRadius = 0;
            borderBottomLeftRadius = 0;
            borderBottomRightRadius = 0;
        }

//        float halfBorderWidth = (borderWidth / 2.0f);

        sPath.reset();
        //start point
        sPath.moveTo(borderWidth + (borderTopLeftRadius > 0 ? borderTopLeftRadius : 0), borderWidth);
        //line top edge
        sPath.lineTo(width - borderWidth - (borderTopRightRadius > 0 ? borderTopRightRadius : 0), borderWidth);
        //arc to right edge
        if (borderTopRightRadius > 0) {
//            oval.set(width - 2 * borderTopRightRadius, 0, width, 2 * borderTopRightRadius);
//            oval.offset(-halfBorderWidth, halfBorderWidth);
//            sPath.arcTo(oval, 270, 90);
            oval.clear();
            oval.fuse(width - 2 * borderTopRightRadius, 0, width, 2 * borderTopRightRadius);
            // sPath.offset(-halfBorderWidth, halfBorderWidth);
            sPath.arcTo(oval, 270, 90);
        }
        //line right edge
        sPath.lineTo(width - borderWidth, height - borderWidth - (borderBottomRightRadius > 0 ? borderBottomRightRadius : 0));
        //arc to bottom edge
        if (borderBottomRightRadius > 0) {
//            oval.set(width - 2 * borderBottomRightRadius, height - 2 * borderBottomRightRadius, width, height);
//            oval.offset(-halfBorderWidth, -halfBorderWidth);
//            sPath.arcTo(oval, 0, 90);

            oval.clear();
            oval.fuse(width - 2 * borderBottomRightRadius, height - 2 * borderBottomRightRadius, width, height);
            // sPath.offset(-halfBorderWidth, -halfBorderWidth);
            sPath.arcTo(oval, 0, 90);
        }
        //line bottom edge
        sPath.lineTo(borderWidth + (borderBottomLeftRadius > 0 ? borderBottomLeftRadius : 0), height - borderWidth);
        //arc to left edge
        if (borderBottomLeftRadius > 0) {
//            oval.set(0, height - 2 * borderBottomLeftRadius, 2 * borderBottomLeftRadius, height);
//            oval.offset(halfBorderWidth, -halfBorderWidth);
//            sPath.arcTo(oval, 90, 90);
            oval.clear();
            oval.fuse(0, height - 2 * borderBottomLeftRadius, 2 * borderBottomLeftRadius, height);
            //   sPath.offset(halfBorderWidth, -halfBorderWidth);
            sPath.arcTo(oval, 90, 90);
        }
        //line left edge
        sPath.lineTo(borderWidth, borderWidth + (borderTopLeftRadius > 0 ? borderTopLeftRadius : 0));
        //arc to top edge
        if (borderTopLeftRadius > 0) {
//            oval.set(0, 0, 2 * borderTopLeftRadius, 2 * borderTopLeftRadius);
//            oval.offset(halfBorderWidth, halfBorderWidth);
//            sPath.arcTo(oval, 180, 90);
            oval.clear();
            oval.fuse(0, 0, 2 * borderTopLeftRadius, 2 * borderTopLeftRadius);
            // sPath.offset(halfBorderWidth, halfBorderWidth);
            sPath.arcTo(oval, 180, 90);
        }
        canvas.drawPath(sPath, sBackgroundPaint);
    }

    public static void clipCanvas(Canvas canvas, int width, int height, int borderWidth,
                                  int borderTopLeftRadius, int borderTopRightRadius, int borderBottomLeftRadius, int borderBottomRightRadius) {
        clipCanvas(null, canvas, width, height, borderWidth, borderTopLeftRadius, borderTopRightRadius, borderBottomLeftRadius, borderBottomRightRadius);
    }

    public static void clipCanvas(Component view, Canvas canvas, int width, int height, int borderWidth,
                                  int borderTopLeftRadius, int borderTopRightRadius, int borderBottomLeftRadius, int borderBottomRightRadius) {
        if (canvas == null) {
            return;
        }

        if (!enableBorderRadius) {
            borderTopLeftRadius = 0;
            borderTopRightRadius = 0;
            borderBottomLeftRadius = 0;
            borderBottomRightRadius = 0;
        }

        if (!isRounded(borderTopLeftRadius, borderTopRightRadius, borderBottomLeftRadius, borderBottomRightRadius)) {
            return;
        }
        sPath.reset();
        //start point
        sPath.moveTo((borderTopLeftRadius > 0 ? borderTopLeftRadius : 0), 0);
        //line top edge
        sPath.lineTo(width - (borderTopRightRadius > 0 ? borderTopRightRadius : 0), 0);
        //arc to right edge
        if (borderTopRightRadius > 0) {
//            oval.set(width - 2 * borderTopRightRadius, 0, width, 2 * borderTopRightRadius);
            oval.clear();
            oval.fuse(width - 2 * borderTopRightRadius, 0, width, 2 * borderTopRightRadius);
            sPath.arcTo(oval, 270, 90);
        }
        //line right edge
        sPath.lineTo(width, height - (borderBottomRightRadius > 0 ? borderBottomRightRadius : 0));
        //arc to bottom edge
        if (borderBottomRightRadius > 0) {
//            oval.set(width - 2 * borderBottomRightRadius, height - 2 * borderBottomRightRadius, width, height);
            oval.clear();
            oval.fuse(width - 2 * borderBottomRightRadius, height - 2 * borderBottomRightRadius, width, height);
            sPath.arcTo(oval, 0, 90);
        }
        //line bottom edge
        sPath.lineTo((borderBottomLeftRadius > 0 ? borderBottomLeftRadius : 0), height);
        //arc to left edge
        if (borderBottomLeftRadius > 0) {
//            oval.set(0, height - 2 * borderBottomLeftRadius, 2 * borderBottomLeftRadius, height);
            oval.clear();
            oval.fuse(0, height - 2 * borderBottomLeftRadius, 2 * borderBottomLeftRadius, height);
            sPath.arcTo(oval, 90, 90);
        }
        //line left edge
        sPath.lineTo(0, (borderTopLeftRadius > 0 ? borderTopLeftRadius : 0));
        //arc to top edge
        if (borderTopLeftRadius > 0) {
//            oval.set(0, 0, 2 * borderTopLeftRadius, 2 * borderTopLeftRadius);
            oval.clear();
            oval.fuse(0, 0, 2 * borderTopLeftRadius, 2 * borderTopLeftRadius);
            sPath.arcTo(oval, 180, 90);
        }
//        if (canvas.isHardwareAccelerated() && (Build.VERSION.SDK_INT < 18) && view != null) {
//            view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        }
//        canvas.clipPath(sPath);
        canvas.clipPath(sPath, Canvas.ClipOp.DIFFERENCE);
    }

    private static boolean isRounded(int borderTopLeftRadius, int borderTopRightRadius, int borderBottomLeftRadius,
                                     int borderBottomRightRadius) {
        return borderTopLeftRadius > 0 || borderTopRightRadius > 0 || borderBottomLeftRadius > 0
                || borderBottomRightRadius > 0;
    }

    public static void drawBackgroundAndBorder(Component component, int backgruondColor,
                                               int borderColor, int borderWidth,
                                               int borderTopLeftRadius, int borderTopRightRadius,
                                               int borderBottomLeftRadius, int borderBottomRightRadius) {
        ShapeElement shapeElement = new ShapeElement();
        float[] radii = new float[]{borderTopLeftRadius, borderTopLeftRadius,
                borderTopRightRadius, borderTopRightRadius,
                borderBottomLeftRadius, borderBottomLeftRadius,
                borderBottomRightRadius, borderBottomRightRadius};
        shapeElement.setCornerRadiiArray(radii);
        if (borderWidth > 0) {
            shapeElement.setStroke(borderWidth, RgbColor.fromArgbInt(borderColor));
        }
        shapeElement.setRgbColor(RgbColor.fromArgbInt(backgruondColor));
        component.setBackground(shapeElement);

    }

}
