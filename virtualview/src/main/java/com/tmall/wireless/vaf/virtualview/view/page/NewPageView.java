package com.tmall.wireless.vaf.virtualview.view.page;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSlider;

import ohos.app.Context;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

/**
 * NewPageImp
 *
 * @author author
 * @version 1.0
 */
public class NewPageView extends PageSlider implements
        ComponentContainer.ArrangeListener,
        Component.EstimateSizeListener,
        Component.BindStateChangedListener,
        PageSlider.PageChangedListener {
    protected Listener mListener;
    protected NewPageSliderProvider mAdapter;
    protected final static int MAX_ITEM_COUNT = 5;
    protected final static int DEFAULT_ANIMATOR_TIME_INTERVAL = 100;
    protected final static int DEFAULT_AUTO_SWITCH_TIME_INTERVAL = 1000;
    protected final static int DEFAULT_STAY_TIME = 2500;
    protected final static int MSG_AUTO_SWITCH = 1;
    protected final static int MSG_ITEM_SWITCH = 2;
    protected final static int VEL_THRESHOLD = 2000;
    protected int mStayTime = DEFAULT_STAY_TIME;
    protected int mAnimatorTimeInterval = DEFAULT_ANIMATOR_TIME_INTERVAL;
    protected int mAutoSwitchTimeInterval = DEFAULT_AUTO_SWITCH_TIME_INTERVAL;
    protected boolean mAutoSwitch = false;
    protected boolean mLayoutNormal = true;
    protected int mAnimationStyle = 0;
    protected boolean mCanSlide = true;
    protected long mAutoSwitchDelay = 0;
    protected boolean mDataChanged = true;
    protected boolean mCanSwitch = true;
    protected EventHandler mAutoSwitchHandler;

    /**
     * NewPageView
     *
     * @param context context
     */
    public NewPageView(Context context) {
        super(context);
        setEstimateSizeListener(this);
        setArrangeListener(this);
        setBindStateChangedListener(this);
        addPageChangedListener(this);
        setPageSwitchTime(mAutoSwitchTimeInterval);
        mAutoSwitchHandler = new EventHandler(EventRunner.getMainEventRunner()) {
            @Override
            protected void processEvent(InnerEvent event) {
                if (MSG_AUTO_SWITCH == event.eventId) {
                    autoSwitch();
                } else if (MSG_ITEM_SWITCH == event.eventId) {
                    int count = getProvider().getCount();
                    int currentPage = getCurrentPage();
                    int last = 0;
                    if (mLayoutNormal) {
                        last = currentPage + 1;
                    } else {
                        last = currentPage - 1;
                        if (last < 0) {
                            last = count - 1;
                        }
                    }
                    setCurrentPage(last, true);
                }
            }
        };
    }

    /**
     * autoSwitch
     */
    public void autoSwitch() {
        mAutoSwitchHandler.removeEvent(MSG_ITEM_SWITCH);
        if (mAutoSwitch && mAdapter.getCount() > 1) {
            mAutoSwitchHandler.sendEvent(MSG_ITEM_SWITCH, mAutoSwitchDelay);
        }
    }

    private void autoSwitchNext() {
        mAutoSwitchHandler.removeEvent(MSG_ITEM_SWITCH);
        //滑动下一个
        if (mAutoSwitch && mAdapter.getCount() > 1) {
            mAutoSwitchHandler.sendEvent(MSG_ITEM_SWITCH, mAutoSwitchTimeInterval + 500);
        }
    }

    /**
     * setListener
     *
     * @param lis lis
     */
    public void setListener(Listener lis) {
        mListener = lis;
    }

    /**
     * setSlide
     *
     * @param slide slide
     */
    public void setSlide(boolean slide) {
        mCanSlide = slide;
        setSlidingPossible(mCanSlide);
    }

    /**
     * setAutoSwitchTimeInterval
     *
     * @param time time
     */
    public void setAutoSwitchTimeInterval(int time) {
        mAutoSwitchTimeInterval = time;
        setPageSwitchTime(time);
    }

    /**
     * setAnimatorTimeInterval
     *
     * @param time time
     */
    public void setAnimatorTimeInterval(int time) {
        mAnimatorTimeInterval = time;
    }

    /**
     * setAnimationStyle
     *
     * @param animationStyle animationStyle
     */
    public void setAnimationStyle(int animationStyle) {
        mAnimationStyle = animationStyle;
    }

    /**
     * setLayoutOrientation
     *
     * @param isNormal isNormal
     */
    public void setLayoutOrientation(boolean isNormal) {
        mLayoutNormal = isNormal;
    }

    /**
     * setStayTime
     *
     * @param time time
     */
    public void setStayTime(int time) {
        mStayTime = time;
    }

    /**
     * setOrientation
     *
     * @param isHorizontal isHorizontal
     */
    public void setOrientation(boolean isHorizontal) {
        setOrientation(isHorizontal ? Component.HORIZONTAL : Component.VERTICAL);
    }

    /**
     * setAutoSwitch
     *
     * @param auto auto
     */
    public void setAutoSwitch(boolean auto) {
        mAutoSwitch = auto;
    }

    /**
     * setAutoSwitchDelay
     *
     * @param delay delay
     */
    public void setAutoSwitchDelay(long delay) {
        this.mAutoSwitchDelay = delay;
    }

    /**
     * refresh
     */
    public void refresh() {
        if (mAutoSwitch && mAdapter.getCount() > 1) {
            mAutoSwitchHandler.removeEvent(MSG_AUTO_SWITCH);
            mAutoSwitchHandler.removeEvent(MSG_ITEM_SWITCH);
            mAutoSwitchHandler.sendEvent(MSG_AUTO_SWITCH, mStayTime);
        }
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        refresh();
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        mAutoSwitchHandler.removeEvent(MSG_AUTO_SWITCH);
        mAutoSwitchHandler.removeEvent(MSG_ITEM_SWITCH);
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        return false;
    }

    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public void onPageSliding(int i, float v, int i1) {
    }

    @Override
    public void onPageSlideStateChanged(int i) {
    }

    @Override
    public void onPageChosen(int i) {
        if (mListener != null) {
            mListener.onPageFlip(i, mAdapter.getCount());
        }
        autoSwitchNext();

    }

    /**
     * Listener
     */
    public interface Listener {
        /**
         * onPageFlip
         *
         * @param pos   pos
         * @param total total
         */
        void onPageFlip(int pos, int total);
    }
}
