/* Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.tmall.wireless.vaf.expr.engine.utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * 弹窗
 */
public class Toast {
    /**
     * 时间
     */
    public static final int LENGTH_SHORT = 2000;

    /**
     * toast的位置
     */
    public enum ToastLayout {
        DEFAULT,
        CENTER,
        TOP,
        BOTTOM,
    }

    /**
     * 弹窗
     *
     * @param mContext 上下文
     * @param content  需要弹窗的内容
     */
    public static void showShort(Context mContext, String content) {
        createTost(mContext, content, LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 弹窗
     *
     * @param mContext 上下文
     * @param content  需要弹窗的内容
     */
    public static void show(Context mContext, String content) {
        createTost(mContext, content, LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 弹窗
     *
     * @param mContext 上下文
     * @param content  需要弹窗的内容
     */
    public static void showShort(Context mContext, int content) {
        createTost(mContext, getString(mContext, content), LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    private static void createTost(Context mContext, String content, int duration, ToastLayout layout) {
        DirectionalLayout toastLayout = new DirectionalLayout(mContext);
        DirectionalLayout.LayoutConfig textConfig = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        Text text = new Text(mContext);
        text.setText(content);
        text.setTextColor(new Color(Color.getIntColor("#ffffff")));
        text.setPadding(vp2px(mContext, 16), vp2px(mContext, 4), vp2px(mContext, 16), vp2px(mContext, 4));
        text.setTextSize(vp2px(mContext, 14));
        text.setBackground(buildDrawableByColorRadius(Color.getIntColor("#70000000"), vp2px(mContext, 20)));
        text.setLayoutConfig(textConfig);
        toastLayout.addComponent(text);
        int mLayout = LayoutAlignment.CENTER;
        switch (layout) {
            case TOP:
                mLayout = LayoutAlignment.TOP;
                break;
            case BOTTOM:
                mLayout = LayoutAlignment.BOTTOM;
                break;
            case CENTER:
                mLayout = LayoutAlignment.CENTER;
                break;
        }
        ToastDialog toastDialog = new ToastDialog(mContext);
        toastDialog.setComponent(toastLayout);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(mLayout);
        toastDialog.setTransparent(true);
        toastDialog.show();
    }

    private static ohos.agp.components.element.Element buildDrawableByColorRadius(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(0);
        drawable.setRgbColor(RgbColor.fromArgbInt(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    private static String getString(Context mContent, int resId) {
        try {
            return mContent.getResourceManager().getElement(resId).getString();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (attributes.densityPixels * vp);
    }
}
