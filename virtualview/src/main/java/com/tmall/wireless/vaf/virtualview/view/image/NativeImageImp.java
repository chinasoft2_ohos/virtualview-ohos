/*
 * MIT License
 *
 * Copyright (c) 2018 Alibaba Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.tmall.wireless.vaf.virtualview.view.image;

import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.app.Context;
import com.tmall.wireless.vaf.virtualview.container.ClickHelper;
import com.tmall.wireless.vaf.virtualview.core.IContainer;
import com.tmall.wireless.vaf.virtualview.core.IView;
import com.tmall.wireless.vaf.virtualview.core.ViewBase;

/**
 * Created by gujicheng on 16/8/16.
 */
public class NativeImageImp extends Image implements IView, IContainer {
    private final static String TAG = "NativeImageImp_TMTEST";

    protected ViewBase mVirtualView;

    public NativeImageImp(Context context) {
        super(context);
    }

    @Override
    public void onComMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        this.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.estimateSize(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void onComLayout(boolean changed, int l, int t, int r, int b) {
//        this.onLayout(changed, l, t, r, b);



//        ComponentContainer.LayoutConfig layoutConfig = getLayoutConfig();
//        layoutConfig.setMargins(l, t, r, b);
//        setLayoutConfig(layoutConfig);
    }

    @Override
    public int getComMeasuredWidth() {
//        return this.getMeasuredWidth();
        return getEstimatedWidth();
    }

    @Override
    public int getComMeasuredHeight() {
//        return this.getMeasuredHeight();
        return getEstimatedHeight();
    }

    @Override
    public void measureComponent(int widthMeasureSpec, int heightMeasureSpec) {
//        this.measure(widthMeasureSpec, heightMeasureSpec);
        this.estimateSize(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void comLayout(int l, int t, int r, int b) {

//        this.layout(l, t, r, b);
        arrange(l, t, r, b);
//        ComponentContainer.LayoutConfig layoutConfig = getLayoutConfig();
//        layoutConfig.setMarginsRelative(l, t, r, b);
//        setLayoutConfig(layoutConfig);
    }

    @Override
    public void attachViews() {
    }

    @Override
    public void setVirtualView(ViewBase view) {
        mVirtualView = view;
        view.setHoldView(this);
        new ClickHelper(this);
    }

    @Override
    public ViewBase getVirtualView() {
        return mVirtualView;
    }

    @Override
    public Component getHolderView() {
        return this;
    }

    @Override
    public void destroy() {
    }

    @Override
    public int getType() {
        return -1;
    }
}
