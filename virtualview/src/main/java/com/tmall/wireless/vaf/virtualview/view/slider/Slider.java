/*
 * MIT License
 *
 * Copyright (c) 2018 Alibaba Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.tmall.wireless.vaf.virtualview.view.slider;


import com.tmall.wireless.vaf.Log;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.ScrollHelper;
import ohos.multimodalinput.event.TouchEvent;
import com.tmall.wireless.vaf.framework.VafContext;
import com.tmall.wireless.vaf.virtualview.core.ViewBase;
import com.tmall.wireless.vaf.virtualview.core.ViewCache;
import com.tmall.wireless.vaf.virtualview.view.scroller.Scroller;
import com.tmall.wireless.vaf.virtualview.view.scroller.ScrollerRecyclerViewAdapter;

import java.util.HashMap;

/**
 * Created by gujicheng on 16/12/15.
 */

public class Slider extends Scroller implements ListContainer.ItemVisibilityChangedListener, Component.ScrolledListener, Component.TouchEventListener {

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_UP:
                int nowScall = mNative.getScrollValue(ScrollHelper.AXIS_X);
                HashMap<Integer, Component> hashMap = ((ScrollerRecyclerViewAdapter) mNative.getItemProvider()).getViews();
                int nowCompomentX = 0;
                for (int i = 0; i < hashMap.size(); i++) {
                    if (nowScall > nowCompomentX && nowScall < nowCompomentX + hashMap.get(i).getWidth() / 2) {
                        mNative.scrollToCenter(i);
                    } else if (nowScall > nowCompomentX + hashMap.get(i).getWidth() / 2 && nowScall < nowCompomentX + hashMap.get(i).getWidth()) {
                        mNative.scrollToCenter(i + 1);
                    }
                    nowCompomentX = nowCompomentX + hashMap.get(i).getWidth();
                }
                break;
        }
        return false;
    }


    public Slider(VafContext context, ViewCache viewCache) {
        super(context, viewCache);

//        LinearSnapHelper snapHelper = new LinearSnapHelper();
//        snapHelper.attachToRecyclerView(mNative);
        mNative.setScrolledListener(this);
        mNative.setTouchEventListener(this);
//        mNative.setDraggedListener(Component.DRAG_HORIZONTAL, this);
//        mNative.setItemSelectedListener(this);
        mNative.addItemVisibilityChangedListener(this);
//        Component.TouchEventListener touchEventListener = mNative.getTouchEventListener();
    }


    @Override
    public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
//        ((ListContainer)component).getItemProvider().getItem()
//        Log.i("Slider.onContentScrolled" + component.getLeft() + "    " + component.getRight() + "     " + component.getTop() + "      " + component.getBottom());
        Log.i("Slider.onContentScrolled " + i + "      " + i1 + "       " + i2 + "         " + i3);
    }


    @Override
    public void onItemAdded(Component component, int i) {
        Log.i("Slider.onItemAdded([component, i]):166    " + i);
    }

    @Override
    public void onItemRemoved(Component component, int j) {
        Log.i("Slider.onItemRemoved([component, j]):213    " + j);
//        if (mNative.getOrientation() == Component.HORIZONTAL) {
//            if (dragState == DragState.right) {
//                mNative.scrollBy(-component1.getWidth(), 0);
//            } else {
//                mNative.scrollBy(component1.getWidth(), 0);
//            }
//        } else {
//            if (dragState == DragState.top) {
//                mNative.scrollBy(0, component1.getHeight());
//            } else {
//                mNative.scrollBy(0, -component1.getHeight());
//            }
//        }
    }


    public static class Builder implements IBuilder {
        @Override
        public ViewBase build(VafContext context, ViewCache viewCache) {
            return new Slider(context, viewCache);
        }
    }

}
