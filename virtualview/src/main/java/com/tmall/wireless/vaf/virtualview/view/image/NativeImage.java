/*
 * MIT License
 *
 * Copyright (c) 2018 Alibaba Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.tmall.wireless.vaf.virtualview.view.image;

import com.libra.virtualview.common.ViewBaseCommon;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.PixelMapElement;
import ohos.media.image.PixelMap;
import com.tmall.wireless.vaf.framework.VafContext;
import com.tmall.wireless.vaf.virtualview.core.ViewBase;
import com.tmall.wireless.vaf.virtualview.core.ViewCache;

import static com.libra.virtualview.common.ViewBaseCommon.AUTO_DIM_DIR_X;
import static com.libra.virtualview.common.ViewBaseCommon.AUTO_DIM_DIR_Y;

/**
 * Created by gujicheng on 16/8/16.
 */
public class NativeImage extends ImageBase {
    private final static String TAG = "NativeImage_TMTEST";

    protected NativeImageImp mNative;

    public NativeImage(VafContext context, ViewCache viewCache) {
        super(context, viewCache);
        mNative = new NativeImageImp(context.forViewConstruction());
    }

    @Override
    public boolean isContainer() {
        return true;
    }

    @Override
    public void setBitmap(PixelMap b, boolean refresh) {
        PixelMapElement pixelMapElement = new PixelMapElement(b);
        mNative.setImageElement(pixelMapElement);
//        mNative.setImageBitmap(b);
    }


    @Override
    public void setSrc(String path) {
        mSrc = path;
        this.mContext.getImageLoader().bindBitmap(mSrc, this, this.getComMeasuredWidth(), this.getComMeasuredHeight());
    }

    @Override
    public Component getNativeView() {
        return mNative;
    }

    @Override
    public void reset() {
        super.reset();
        this.mContext.getImageLoader().bindBitmap(null, this, this.getComMeasuredWidth(), this.getComMeasuredHeight());
    }

    @Override
    public void onParseValueFinished() {
        super.onParseValueFinished();
//        mNative.setScaleType(ImageBase.IMAGE_SCALE_TYPE.get(mScaleType));

        mNative.setScaleMode(ImageBase.IMAGE_SCALE_TYPE.get(mScaleType, Image.ScaleMode.STRETCH));
        setSrc(this.mSrc);
    }

    @Override
    public void measureComponent(int widthMeasureSpec, int heightMeasureSpec) {
        if (mAutoDimDirection > 0) {
            switch (mAutoDimDirection) {
                case AUTO_DIM_DIR_X:
//                    if (Component.EstimateSpec.EXACTLY == Component.EstimateSpec.getMode(widthMeasureSpec)) {
//                        heightMeasureSpec = Component.EstimateSpec.makeMeasureSpec((int) ((Component.EstimateSpec.getSize(widthMeasureSpec) * mAutoDimY) / mAutoDimX), Component.EstimateSpec.EXACTLY);
//                    }
                    if (Component.EstimateSpec.PRECISE == Component.EstimateSpec.getMode(widthMeasureSpec)) {
                        heightMeasureSpec = Component.EstimateSpec.getSizeWithMode((int) ((Component.EstimateSpec.getSize(widthMeasureSpec) * mAutoDimY) / mAutoDimX), Component.EstimateSpec.PRECISE);
                    }
                    break;

                case AUTO_DIM_DIR_Y:
//                    if (Component.EstimateSpec.EXACTLY == Component.EstimateSpec.getMode(heightMeasureSpec)) {
//                        widthMeasureSpec = Component.EstimateSpec.makeMeasureSpec((int) ((Component.EstimateSpec.getSize(heightMeasureSpec) * mAutoDimX) / mAutoDimY), Component.EstimateSpec.EXACTLY);
//                    }

                    if (Component.EstimateSpec.PRECISE == Component.EstimateSpec.getMode(heightMeasureSpec)) {
                        widthMeasureSpec = Component.EstimateSpec.getSizeWithMode((int) ((Component.EstimateSpec.getSize(heightMeasureSpec) * mAutoDimX) / mAutoDimY), Component.EstimateSpec.PRECISE);
                    }
                    break;
                default:
                    break;
            }
        }
        mNative.measureComponent(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void onComMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mAutoDimDirection > 0) {
            switch (mAutoDimDirection) {
                case ViewBaseCommon.AUTO_DIM_DIR_X:
//                    if (Component.EstimateSpec.EXACTLY == Component.EstimateSpec.getMode(widthMeasureSpec)) {
//                        heightMeasureSpec = Component.EstimateSpec.makeMeasureSpec((int) ((Component.EstimateSpec.getSize(widthMeasureSpec) * mAutoDimY) / mAutoDimX), Component.EstimateSpec.EXACTLY);
//                    }

                    if (Component.EstimateSpec.PRECISE == Component.EstimateSpec.getMode(widthMeasureSpec)) {
                        heightMeasureSpec = Component.EstimateSpec.getSizeWithMode((int) ((Component.EstimateSpec.getSize(widthMeasureSpec) * mAutoDimY) / mAutoDimX), Component.EstimateSpec.PRECISE);
                    }
                    break;

                case ViewBaseCommon.AUTO_DIM_DIR_Y:
//                    if (Component.EstimateSpec.EXACTLY == Component.EstimateSpec.getMode(heightMeasureSpec)) {
//                        widthMeasureSpec = Component.EstimateSpec.makeMeasureSpec((int) ((Component.EstimateSpec.getSize(heightMeasureSpec) * mAutoDimX) / mAutoDimY), Component.EstimateSpec.EXACTLY);
//                    }

                    if (Component.EstimateSpec.PRECISE == Component.EstimateSpec.getMode(heightMeasureSpec)) {
                        widthMeasureSpec = Component.EstimateSpec.getSizeWithMode((int) ((Component.EstimateSpec.getSize(heightMeasureSpec) * mAutoDimX) / mAutoDimY), Component.EstimateSpec.PRECISE);
                    }
                    break;
                default:
                    break;
            }
        }

        mNative.onComMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void onComLayout(boolean changed, int l, int t, int r, int b) {
        mNative.onComLayout(changed, l, t, r, b);
    }

    @Override
    public int getComMeasuredWidth() {
        return mNative.getComMeasuredWidth();
    }

    @Override
    public int getComMeasuredHeight() {
        return mNative.getComMeasuredHeight();
    }

    @Override
    public void comLayout(int l, int t, int r, int b) {
        super.comLayout(l, t, r, b);
        mNative.comLayout(l, t, r, b);
    }

    public static class Builder implements IBuilder {
        @Override
        public ViewBase build(VafContext context, ViewCache viewCache) {
            return new NativeImage(context, viewCache);
        }
    }

}
