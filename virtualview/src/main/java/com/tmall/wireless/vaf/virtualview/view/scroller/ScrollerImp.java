package com.tmall.wireless.vaf.virtualview.view.scroller;

import com.libra.virtualview.common.ScrollerCommon;
import com.tmall.wireless.vaf.Log;
import com.tmall.wireless.vaf.virtualview.core.IView;
import ohos.agp.components.ListContainer;
import com.tmall.wireless.vaf.framework.VafContext;

public class ScrollerImp extends ListContainer {
    protected VafContext mAppContext;
    protected Scroller mScroller;
    protected ScrollerRecyclerViewAdapter mAdapter;


    public ScrollerImp(VafContext vafContext, Scroller scroller) {
        super(vafContext.forViewConstruction());
        mAppContext = vafContext;
        mScroller = scroller;
        mAdapter = new ScrollerRecyclerViewAdapter(vafContext, this);
        setItemProvider(mAdapter);

        vafContext.forViewConstruction().getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                Log.i("ScrollerImp.run([]):25   "+ScrollerImp.this.getWidth());
                ScrollerImp.this.getItemProvider().notifyDataChanged();
            }
        },1000);
    }

    public void setData(Object str) {
        // parse data
        ((ScrollerRecyclerViewAdapter) getItemProvider()).setData(str);
        getItemProvider().notifyDataChanged();
    }

    public void appendData(Object data) {
        mAdapter.appendData(data);
    }

    public void destroy() {
        mScroller = null;
        mAdapter.destroy();
        mAdapter = null;
    }

    public void setAutoRefreshThreshold(int threshold) {
        mAdapter.setAutoRefreshThreshold(threshold);
    }

    public void callAutoRefresh() {
        mScroller.callAutoRefresh();
    }


    public void setModeOrientation(int mode, int orientation) {
        switch (mode) {
            case ScrollerCommon.MODE_StaggeredGrid:
                setOrientation(orientation);
                break;
            default:
                Log.i("mode invalidate:" + mode);
                break;
        }
    }



}
