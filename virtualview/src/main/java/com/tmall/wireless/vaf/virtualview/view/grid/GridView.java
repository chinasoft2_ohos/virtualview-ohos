/*
 * MIT License
 *
 * Copyright (c) 2018 Alibaba Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.tmall.wireless.vaf.virtualview.view.grid;


import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;
import com.tmall.wireless.vaf.virtualview.Helper.RtlHelper;

import static com.libra.virtualview.common.ViewBaseCommon.*;

/**
 * Created by gujicheng on 16/10/27.
 */
public class GridView extends ComponentContainer implements ComponentContainer.ArrangeListener, Component.EstimateSizeListener {
    private final static String TAG = "GridView_TMTEST";

    protected final static int DEFAULT_COLUMN_COUNT = 2;
    protected final static int DEFAULT_ITEM_HEIGHT = 0;

    protected int mColumnCount = DEFAULT_COLUMN_COUNT;
    protected int mRowCount;
    protected int mItemHeight = DEFAULT_ITEM_HEIGHT;
    protected int mItemWidth;
    protected int mCalItemHeight;

    protected int mItemHorizontalMargin = 0;
    protected int mItemVerticalMargin = 0;

    protected int mAutoDimDirection = AUTO_DIM_DIR_NONE;
    protected float mAutoDimX = 1;
    protected float mAutoDimY = 1;

    public GridView(Context context) {
        super(context);
        setArrangeListener(this);
        setEstimateSizeListener(this);
    }

    public void setAutoDimDirection(int direction) {
        mAutoDimDirection = direction;
    }

    public void setAutoDimX(float dimX) {
        mAutoDimX = dimX;
    }

    public void setAutoDimY(float dimY) {
        mAutoDimY = dimY;
    }

    public void setColumnCount(int count) {
        mColumnCount = count;
    }

    public void setItemHorizontalMargin(int margin) {
        mItemHorizontalMargin = margin;
    }

    public void setItemVerticalMargin(int margin) {
        mItemVerticalMargin = margin;
    }

    public void setItemHeight(int h) {
        mItemHeight = h;
    }


    //    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mAutoDimDirection > 0) {
            switch (mAutoDimDirection) {
                case AUTO_DIM_DIR_X:
//                    if (Component.EstimateSpec.EXACTLY == Component.EstimateSpec.getMode(widthMeasureSpec)) {
//                        heightMeasureSpec = Component.EstimateSpec.makeMeasureSpec((int) ((Component.EstimateSpec.getSize(widthMeasureSpec) * mAutoDimY) / mAutoDimX), Component.EstimateSpec.EXACTLY);
//                    }
                    if (EstimateSpec.PRECISE == EstimateSpec.getMode(widthMeasureSpec)) {
                        heightMeasureSpec = EstimateSpec.getSizeWithMode((int) ((EstimateSpec.getSize(widthMeasureSpec) * mAutoDimY) / mAutoDimX), EstimateSpec.PRECISE);
                    }
                    break;

                case AUTO_DIM_DIR_Y:
//                    if (Component.EstimateSpec.EXACTLY == Component.EstimateSpec.getMode(heightMeasureSpec)) {
//                        widthMeasureSpec = Component.EstimateSpec.makeMeasureSpec((int) ((Component.EstimateSpec.getSize(heightMeasureSpec) * mAutoDimX) / mAutoDimY), Component.EstimateSpec.EXACTLY);
//                    }
                    if (EstimateSpec.PRECISE == EstimateSpec.getMode(heightMeasureSpec)) {
                        widthMeasureSpec = EstimateSpec.getSizeWithMode((int) ((EstimateSpec.getSize(heightMeasureSpec) * mAutoDimX) / mAutoDimY), EstimateSpec.PRECISE);
                    }
                    break;
            }
        }

        int width = EstimateSpec.getSize(widthMeasureSpec);
        int height = EstimateSpec.getSize(heightMeasureSpec);

//        int widthMode = EstimateSpec.getMode(widthMeasureSpec);
        int heightMode = EstimateSpec.getMode(heightMeasureSpec);

        int space = this.getPaddingLeft() + this.getPaddingRight() + mItemHorizontalMargin * (mColumnCount - 1);

        int childCount = this.getChildCount();

//        Log.d(TAG, "width:" + width+ "  widthMode:" + widthMode + "  height:" + height + "  heightMode:" + heightMode);
        mItemWidth = (width - space) / mColumnCount;
        int index = 0;
        if (mItemHeight <= 0) {
            if (childCount > 0) {
//                View child = this.getChildAt(index++);
                Component child = this.getComponentAt(index++);
//                child.measure(Component.EstimateSpec.makeMeasureSpec(mItemWidth, Component.EstimateSpec.EXACTLY), heightMeasureSpec);
                child.estimateSize(EstimateSpec.getSizeWithMode(mItemWidth, EstimateSpec.PRECISE), heightMeasureSpec);
//                mCalItemHeight = child.getMeasuredHeight();
                mCalItemHeight = child.getEstimatedHeight();
            } else {
                mCalItemHeight = height - this.getPaddingTop() - this.getPaddingBottom();
            }
        } else {
            mCalItemHeight = mItemHeight;
        }

//        Log.d(TAG, "mCalItemHeight:" + mCalItemHeight + "mItemWidth:" + mItemWidth);

//        int widthSpec = Component.EstimateSpec.makeMeasureSpec(mItemWidth, Component.EstimateSpec.EXACTLY);
//        int heightSpec = Component.EstimateSpec.makeMeasureSpec(mCalItemHeight, Component.EstimateSpec.EXACTLY);
        int widthSpec = EstimateSpec.getSizeWithMode(mItemWidth, EstimateSpec.PRECISE);
        int heightSpec = EstimateSpec.getSizeWithMode(mCalItemHeight, EstimateSpec.PRECISE);
        for (int i = index; i < childCount; ++i) {
//            final View child = this.getChildAt(i);
//            child.measure(widthSpec, heightSpec);
            final Component child = this.getComponentAt(i);
            child.estimateSize(widthSpec, heightSpec);
        }

        mRowCount = (childCount / mColumnCount) + ((childCount % mColumnCount) > 0 ? 1 : 0);
        int h = mRowCount * mCalItemHeight + (mRowCount - 1) * mItemVerticalMargin + this.getPaddingTop() + this.getPaddingBottom();

        int realHeight;
//        if (MeasureSpec.UNSPECIFIED == heightMode) {
        if (EstimateSpec.UNCONSTRAINT == heightMode) {
            realHeight = h;
        } else {
            realHeight = Math.min(height, h);
        }

//        setMeasuredDimension(width, realHeight);
        setEstimatedSize(width, realHeight);
    }

    //    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = this.getChildCount();

        int index = 0;
        int top = this.getPaddingTop();
        int paddingLeft = this.getPaddingLeft();
        for (int row = 0; row < mRowCount; ++row) {
            int left = paddingLeft;
            for (int col = 0; col < mColumnCount; ++col) {
                if (index < count) {
//                    View child = this.getChildAt(index);
                    Component child = this.getComponentAt(index);

                    int realLeft = RtlHelper.getRealLeft(RtlHelper.isRtl(), 0, (r - l), left, mItemWidth);
//                    child.layout(realLeft, top, realLeft + mItemWidth, top + mCalItemHeight);
                    child.arrange(realLeft, top, realLeft + mItemWidth, top + mCalItemHeight);
                } else {
                    break;
                }
                ++index;
                left += mItemWidth + mItemHorizontalMargin;
            }
            top += mCalItemHeight + mItemVerticalMargin;
        }
    }


    //TODO 新增
    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        onLayout(true, i, i1, i2, i3);
        return false;
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        onMeasure(i, i1);
        return true;
    }
}
