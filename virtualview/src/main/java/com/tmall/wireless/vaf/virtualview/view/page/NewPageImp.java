/*
 * MIT License
 *
 * Copyright (c) 2018 Alibaba Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.tmall.wireless.vaf.virtualview.view.page;

import com.alibaba.fastjson.JSONArray;

import ohos.agp.components.Component;

import com.tmall.wireless.vaf.framework.VafContext;
import com.tmall.wireless.vaf.virtualview.core.IContainer;
import com.tmall.wireless.vaf.virtualview.core.IView;
import com.tmall.wireless.vaf.virtualview.core.ViewBase;

/**
 * NewPageImp
 *
 * @author author
 * @version 1.0
 */
public class NewPageImp extends NewPageView implements IView, IContainer {
    private final static String TAG = "PageImp_TMTEST";

    protected ViewBase mVirtualView;

    /**
     * NewPageImp
     *
     * @param context
     */
    public NewPageImp(VafContext context) {
        super(context.forViewConstruction());
        mAdapter = new NewPageSliderProvider(context);
        setProvider(mAdapter);
    }

    /**
     * setContainerId
     *
     * @param id
     */
    public void setContainerId(int id) {
        mAdapter.setContainerId(id);
    }

    /**
     * reset
     */
    public void reset() {
        mAdapter.setData(null);
    }

    /**
     * setData
     *
     * @param data data
     */
    public void setData(Object data) {
        if (data instanceof JSONArray) {
            mAdapter.setData((JSONArray) data);
        }
    }

    /**
     * size
     *
     * @return  int
     */
    public int size() {
        return mAdapter.getCount();
    }

    /**
     * measureComponent
     *
     * @param widthMeasureSpec  widthMeasureSpec
     * @param heightMeasureSpec heightMeasureSpec
     */
    @Override
    public void measureComponent(int widthMeasureSpec, int heightMeasureSpec) {
        this.estimateSize(widthMeasureSpec, heightMeasureSpec);
    }

    /**
     * comLayout
     *
     * @param l l
     * @param t t
     * @param r r
     * @param b b
     */
    @Override
    public void comLayout(int l, int t, int r, int b) {
        arrange(l, t, r, b);
    }

    /**
     * onComMeasure
     *
     * @param widthMeasureSpec  widthMeasureSpec
     * @param heightMeasureSpec heightMeasureSpec
     */
    @Override
    public void onComMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        this.onEstimateSize(widthMeasureSpec, heightMeasureSpec);
    }

    /**
     * onComLayout
     *
     * @param changed changed
     * @param l       l
     * @param t       t
     * @param r       r
     * @param b       b
     */
    @Override
    public void onComLayout(boolean changed, int l, int t, int r, int b) {
        this.onArrange(l, t, r, b);
    }

    /**
     * getComMeasuredWidth
     *
     * @return int
     */
    @Override
    public int getComMeasuredWidth() {
        return this.getEstimatedWidth();

    }

    /**
     * getComMeasuredHeight
     *
     * @return int
     */
    @Override
    public int getComMeasuredHeight() {
        return this.getEstimatedHeight();
    }

    /**
     * attachViews
     */
    @Override
    public void attachViews() {
    }

    /**
     * setVirtualView
     *
     * @param view view
     */
    @Override
    public void setVirtualView(ViewBase view) {
        mVirtualView = view;
    }

    /**
     * getVirtualView
     *
     * @return ViewBase
     */
    @Override
    public ViewBase getVirtualView() {
        return mVirtualView;
    }

    /**
     * getHolderView
     *
     * @return Component
     */
    @Override
    public Component getHolderView() {
        return null;
    }

    /**
     * destroy
     */
    @Override
    public void destroy() {
    }

    /**
     * getType
     *
     * @return int
     */
    @Override
    public int getType() {
        return -1;
    }
}
