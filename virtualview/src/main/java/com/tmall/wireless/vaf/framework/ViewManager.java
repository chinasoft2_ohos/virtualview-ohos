/*
 * MIT License
 *
 * Copyright (c) 2018 Alibaba Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.tmall.wireless.vaf.framework;

import com.libra.TextUtils;
import com.tmall.wireless.vaf.Log;
import ohos.app.Context;
import ohos.utils.PlainArray;
import com.tmall.wireless.vaf.virtualview.ViewFactory;
import com.tmall.wireless.vaf.virtualview.core.Layout;
import com.tmall.wireless.vaf.virtualview.core.ViewBase;
import com.tmall.wireless.vaf.virtualview.core.ViewCache;
import com.tmall.wireless.vaf.virtualview.view.image.VirtualImage;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * Created by gujicheng on 16/9/22.
 */
public class ViewManager {
    private final static String TAG = "ViewManager_TMTEST";

    private ViewFactory mViewFactory = new ViewFactory();

    private ConcurrentHashMap<String, List<ViewBase>> mViewCache = new ConcurrentHashMap<>();
    private PlainArray<ViewBase> mUuidContainers = new PlainArray<>();

    private VafContext mAppContext;

    public void setPageContext(VafContext context) {
        mAppContext = context;
        mViewFactory.setPageContext(context);
    }

    public ViewFactory getViewFactory() {
        return mViewFactory;
    }

    public boolean init(Context context) {
        return mViewFactory.init(context);
    }

    public int loadBinFileSync(String path) {
        return mViewFactory.loadBinFile(path);
    }

    public int loadBinBufferSync(byte[] buffer) {
        return mViewFactory.loadBinBuffer(buffer);
    }

    public int loadBinBufferSync(byte[] buffer, boolean override) {
        return mViewFactory.loadBinBuffer(buffer, override);
    }

    public void loadBinBufferAsync(String type, byte[] buffer) {
        mViewFactory.loadBinBufferAsync(type, buffer);
    }

    public void loadBinBufferAsync(String type, byte[] buffer, boolean override) {
        mViewFactory.loadBinBufferAsync(type, buffer, override);
    }

    public ViewBase getViewFromUuid(int uuid) {
        return mUuidContainers.get(uuid).get();
    }

    public void destroy() {
        for (Map.Entry<String, List<ViewBase>> entry : mViewCache.entrySet()) {
            List<ViewBase> viewBases = entry.getValue();
            if (null != viewBases) {
                for (int j = 0; j < viewBases.size(); ++j) {
                    ViewBase viewBase = viewBases.get(j);
                    viewBase.destroy();
                    ViewCache viewCache = viewBase.getViewCache();
                    if (viewCache != null) {
                        viewCache.destroy();
                    }
                }
                viewBases.clear();
            }
        }
        mViewCache.clear();
        mViewCache = null;
        mViewFactory.destroy();
        mUuidContainers.clear();
        mUuidContainers = null;
    }

    public ViewBase getDefaultImage() {
        ViewBase vb = new VirtualImage(mAppContext, new ViewCache());
        vb.setComLayoutParams(new Layout.Params());
        return vb;
    }

    public int getViewVersion(String type) {
        return mViewFactory.getViewVersion(type);
    }

    public ViewBase getView(String type) {
        ViewBase v;
        List<ViewBase> vList = mViewCache.get(type);
        if (null == vList || 0 == vList.size()) {
            v = mViewFactory.newView(type, mUuidContainers);
            if (null != v) {
                if (v.supportDynamic()) {
                    mAppContext.getNativeObjectManager().addView(v);
                }
                v.setViewType(type);
            } else {
                Log.e(TAG, "new view failed type:" + type);
            }
        } else {
            v = vList.remove(0);
        }

        return v;
    }

    public void recycle(ViewBase v) {
        if (null != v) {
            String type = v.getViewType();
            if (!TextUtils.isEmpty(type)) {
                v.reset();
                List<ViewBase> vList = null;
                if (mViewCache != null && mViewCache.size() > 0) {
                    vList = mViewCache.computeIfAbsent(type, new Function<String, List<ViewBase>>() {
                        @Override
                        public List<ViewBase> apply(String s) {
                            LinkedList vList = new LinkedList<>();
                            mViewCache.put(type, vList);
                            return vList;
                        }
                    });
                }
                if (null == vList) {
                    vList = new LinkedList<>();
                    mViewCache.put(type, vList);
                }
                vList.add(v);
            } else {
                Log.e(TAG, "recycle type invalidate:" + type);
                RuntimeException here = new RuntimeException("here");
                here.fillInStackTrace();
                Log.e(TAG, "Called: " + this + "           " + here.toString());
            }
        }
    }
}
