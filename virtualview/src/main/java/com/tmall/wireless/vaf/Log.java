/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.tmall.wireless.vaf;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * 日志打印
 */
public class Log {
    public static final HiLogLabel hiLogLabel = new HiLogLabel(HiLog.INFO, 0x001, "HMOS");
    /**
     * 日志打印
     * @param tag 标示
     * @param info 信息
     * @return 信息
     */
    public static String i(String tag, String info) {
        HiLog.info(hiLogLabel, tag, info);
        Logger.getLogger(tag).info(info);
        return info;
    }

    /**
     * 日志打印
     * @param tag 标示
     * @param info 信息
     * @return 信息
     */
    public static String d(String tag, String info) {
        HiLog.info(hiLogLabel, tag, info);
        Logger.getLogger(tag).info(info);
        return info;
    }

    /**
     * 日志打印
     * @param info 信息
     * @return 信息
     */
    public static String i(String info) {
        HiLog.info(hiLogLabel, "test", info);
        Logger.getLogger("test").info(info);
        return info;
    }

    /**
     * 日志打印
     * @param tag 标示
     * @param info 信息
     * @return 信息
     */
    public static String e(String tag, String info) {
        HiLog.error(hiLogLabel, tag, info);
        Logger.getLogger(tag).warning(info);
        return info;
    }

    /**
     * 日志打印
     * @param info 提示信息
     * @param thro 错误信息
     * @return 提示信息
     */
    public static String e(String info, Throwable thro) {
        HiLog.error(hiLogLabel, "error", info + "error:" + thro.getMessage());
        return info;
    }


    static String Tag = "开始";

    /**
     * 日志打印状态
     */
    public enum MethodStatus {
        start, complate
    }

    /**
     * 日志打印
     * @param methodName  方法名
     * @param methodStatus  方法执行状态
     */
    public static void o(String methodName, MethodStatus methodStatus) {
        if (methodStatus == MethodStatus.start) {
            if ("结束".equals(Tag)) {
                Tag = "开始";
            }
            Tag += "->" + methodName;
            Log.e("示例", "----------------" + Tag + "          " + Thread.currentThread());
        } else {
            String[] strings = Tag.split("->");
            List<String> tagList = new ArrayList<>();
            for (int i = 0; i < strings.length; i++) {
                tagList.add(strings[i]);
            }

            if (tagList.get(tagList.size() - 1).contains(methodName)) {
                tagList.remove(tagList.size() - 1);
            }
            Tag = "";
            for (int i = 0; i < tagList.size(); i++) {
                if ("".equals(Tag)) {
                    Tag = tagList.get(i);
                } else {
                    Tag += "->" + tagList.get(i);
                }
            }
            if ("开始".equals(Tag)) {
                Tag = "结束";
            }
            Log.e("error", "----------------" + Tag + "->" + methodName + "error" + "          " + Thread.currentThread());
        }
    }
}
