package com.tmall.wireless.virtualviewdemo;

import com.tmall.wireless.vaf.virtualview.event.EventData;
import com.tmall.wireless.vaf.virtualview.event.EventManager;
import com.tmall.wireless.vaf.virtualview.event.IEventProcessor;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @时间：
 * @描述：
 **/
public class EventManagerTest {
    EventManager eventManager = new EventManager();
    IEventProcessor eventProcessor = new IEventProcessor() {
        @Override
        public boolean process(EventData data) {
            return false;
        }
    };

    /**
     * 注册
     */
    @Test
    public void register() {
        eventManager.register(-1, eventProcessor);
        eventManager.register(1, eventProcessor);
        eventManager.register(Integer.MAX_VALUE + 1, eventProcessor);
    }

    /**
     *注销
     */
    @Test
    public void unregister() {
        eventManager.unregister(-1, eventProcessor);
        eventManager.unregister(1, eventProcessor);
        eventManager.unregister(Integer.MAX_VALUE + 1, eventProcessor);
    }
}