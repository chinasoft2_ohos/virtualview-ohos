package com.tmall.wireless.virtualviewdemo;

import com.tmall.wireless.vaf.framework.UserData;

import org.junit.Test;

/**
 * @时间：2021/06/11
 * @描述：
 **/
public class UserDataTest {
    UserData userData = new UserData();

    /**
     * 清除数据
     */
    @Test
    public void clear() {
        userData.clear();
    }

    /**
     * 添加数据
     */
    @Test
    public void put() {
        userData.put(null, "test");
        userData.put("test", null);
        userData.put("test", "test");
    }

    /**
     * 获取数据
     */
    @Test
    public void get() {
        Object test = userData.get("test");
    }
}