package com.tmall.wireless.virtualviewdemo.slice;

import com.tmall.wireless.virtualviewdemo.ComponentAbility;
import com.tmall.wireless.virtualviewdemo.ResourceTable;
import com.tmall.wireless.virtualviewdemo.provider.MainListAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

public class TmallComponentListAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_tmall_component_list);
        initView();
    }

    private void initView() {

        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        List<MainListAdapter.MainItemEntity> list = new ArrayList<>();


        MainListAdapter.MainItemEntity component1 = new MainListAdapter.MainItemEntity();
        component1.name = "TmallComponent1";
        component1.clasz = ComponentAbility.class.getCanonicalName();
        component1.data = "component_demo/tmall_component_1.json";
        list.add(component1);
        MainListAdapter.MainItemEntity component2 = new MainListAdapter.MainItemEntity();
        component2.name = "TmallComponent2";
        component2.clasz = ComponentAbility.class.getCanonicalName();
        component2.data = "component_demo/tmall_component_2.json";
        list.add(component2);
        MainListAdapter.MainItemEntity component3 = new MainListAdapter.MainItemEntity();
        component3.name = "TmallComponent3";
        component3.clasz = ComponentAbility.class.getCanonicalName();
        component3.data = "component_demo/tmall_component_3.json";
        list.add(component3);
        MainListAdapter.MainItemEntity component4 = new MainListAdapter.MainItemEntity();
        component4.name = "TmallComponent4";
        component4.clasz = ComponentAbility.class.getCanonicalName();
        component4.data = "component_demo/tmall_component_4.json";
        list.add(component4);
        MainListAdapter.MainItemEntity component5 = new MainListAdapter.MainItemEntity();
        component5.name = "TmallComponent5";
        component5.clasz = ComponentAbility.class.getCanonicalName();
        component5.data = "component_demo/tmall_component_5.json";
        list.add(component5);
        MainListAdapter.MainItemEntity component6 = new MainListAdapter.MainItemEntity();
        component6.name = "TmallComponent6";
        component6.clasz = ComponentAbility.class.getCanonicalName();
        component6.data = "component_demo/tmall_component_6.json";
        list.add(component6);
        MainListAdapter.MainItemEntity component7 = new MainListAdapter.MainItemEntity();
        component7.name = "TmallComponent7";
        component7.clasz = ComponentAbility.class.getCanonicalName();
        component7.data = "component_demo/tmall_component_7.json";
        list.add(component7);
        MainListAdapter.MainItemEntity component8 = new MainListAdapter.MainItemEntity();
        component8.name = "TmallComponent8";
        component8.clasz = ComponentAbility.class.getCanonicalName();
        component8.data = "component_demo/tmall_component_8.json";
        list.add(component8);
        MainListAdapter.MainItemEntity picasso = new MainListAdapter.MainItemEntity();
        picasso.name = "Picasso";
        picasso.clasz = ComponentAbility.class.getCanonicalName();
        picasso.data = "component_demo/picasso.json";
        list.add(picasso);


        MainListAdapter adapter = new MainListAdapter(this, list);
        listContainer.setItemProvider(adapter);

        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                MainListAdapter.MainItemEntity mainItemEntity = (MainListAdapter.MainItemEntity) listContainer.getItemProvider().getItem(i);
                Intent getIntent = new Intent();
                Operation opt = new Intent
                        .OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(mainItemEntity.clasz)
                        .build();
                getIntent.setParam("data", mainItemEntity.data);
                getIntent.setParam("name", mainItemEntity.name);
                getIntent.setOperation(opt);
                getAbility().startAbility(getIntent);
            }
        });
    }
}
