package com.tmall.wireless.virtualviewdemo;

import com.tmall.wireless.virtualviewdemo.slice.PreviewActivityAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class PreviewActivityAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PreviewActivityAbilitySlice.class.getName());
    }
}
