package com.tmall.wireless.virtualviewdemo;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;

public class PreviewData {
    public ArrayList<String> templates;
    public JSONObject data;
}
