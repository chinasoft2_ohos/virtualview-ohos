package com.tmall.wireless.virtualviewdemo;

import com.tmall.wireless.virtualviewdemo.slice.ComponentListActivitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ComponentListAbility extends Ability {
    @Override
    public final void onStart(final Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ComponentListActivitySlice.class.getName());
    }
}
