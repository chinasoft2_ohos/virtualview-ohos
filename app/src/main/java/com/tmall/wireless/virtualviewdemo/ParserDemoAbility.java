package com.tmall.wireless.virtualviewdemo;

import com.tmall.wireless.virtualviewdemo.slice.ParserDemoAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ParserDemoAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ParserDemoAbilitySlice.class.getName());
    }
}
