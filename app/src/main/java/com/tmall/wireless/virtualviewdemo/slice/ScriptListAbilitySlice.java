package com.tmall.wireless.virtualviewdemo.slice;

import com.tmall.wireless.vaf.expr.engine.utils.Toast;
import com.tmall.wireless.virtualviewdemo.ComponentAbility;
import com.tmall.wireless.virtualviewdemo.MyApplication;
import com.tmall.wireless.virtualviewdemo.ResourceTable;
import com.tmall.wireless.virtualviewdemo.provider.MainListAdapter;
import com.tmall.wireless.virtualviewdemo.vs.VSEngine;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.TextTool;

import java.util.ArrayList;
import java.util.List;

public class ScriptListAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_script_list);
        initView();
    }

    private void initView() {

        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        List<MainListAdapter.MainItemEntity> list = new ArrayList<>();


        MainListAdapter.MainItemEntity ntext = new MainListAdapter.MainItemEntity();
        ntext.name = "this.name = (data.time > data.id) && (data.time >= 3)";
        list.add(ntext);
        MainListAdapter.MainItemEntity page = new MainListAdapter.MainItemEntity();
        page.name = "PageScrollScript";
        page.clasz = ComponentAbility.class.getCanonicalName();
        page.data = "component_demo/page_item.json";
        list.add(page);
        MainListAdapter.MainItemEntity click = new MainListAdapter.MainItemEntity();
        click.name = "ClickScript";
        click.clasz = ComponentAbility.class.getCanonicalName();
        list.add(click);

        MainListAdapter adapter = new MainListAdapter(this, list);
        listContainer.setItemProvider(adapter);
        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                MainListAdapter.MainItemEntity mainItemEntity = (MainListAdapter.MainItemEntity) listContainer.getItemProvider().getItem(i);
                if (TextTool.isNullOrEmpty(mainItemEntity.clasz)) {
                    String result = VSEngine.test();
                    Toast.showShort(MyApplication.getInstance().getContext(), "executing result: " + result);
                } else {
                    Intent getIntent = new Intent();
                    Operation opt = new Intent
                            .OperationBuilder()
                            .withBundleName(getBundleName())
                            .withAbilityName(mainItemEntity.clasz)
                            .build();
                    getIntent.setParam("data", mainItemEntity.data);
                    getIntent.setParam("name", mainItemEntity.name);
                    getIntent.setOperation(opt);
                    getAbility().startAbility(getIntent);
                }
            }
        });

    }


}
