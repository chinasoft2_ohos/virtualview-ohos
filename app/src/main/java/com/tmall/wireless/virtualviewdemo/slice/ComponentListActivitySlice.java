package com.tmall.wireless.virtualviewdemo.slice;

import com.tmall.wireless.vaf.Log;
import com.tmall.wireless.vaf.expr.engine.utils.Toast;
import com.tmall.wireless.virtualviewdemo.ComponentAbility;
import com.tmall.wireless.virtualviewdemo.ResourceTable;
import com.tmall.wireless.virtualviewdemo.provider.MainListAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

public class ComponentListActivitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_component_list_activity);
        initView();
    }


    private void initView() {
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        List<MainListAdapter.MainItemEntity> list = new ArrayList<>();
        List<MainListAdapter.MainItemEntity> notSupportlist = new ArrayList<>();

        MainListAdapter.MainItemEntity ntext = new MainListAdapter.MainItemEntity();
        ntext.name = "NText";
        ntext.data = "component_demo/ntext_style.json";
        ntext.clasz = ComponentAbility.class.getCanonicalName();
        list.add(ntext);

        MainListAdapter.MainItemEntity vtext = new MainListAdapter.MainItemEntity();
        vtext.name = "VText";
        vtext.clasz = ComponentAbility.class.getCanonicalName();
        list.add(vtext);
//        MainListAdapter.MainItemEntity tmnimage = new MainListAdapter.MainItemEntity();
//        tmnimage.name = "TMNImage";
//        tmnimage.clasz = ComponentAbility.class.getCanonicalName();
//        list.add(tmnimage);
        MainListAdapter.MainItemEntity nimage = new MainListAdapter.MainItemEntity();
        nimage.name = "NImage";
        nimage.clasz = ComponentAbility.class.getCanonicalName();
        list.add(nimage);
        MainListAdapter.MainItemEntity vimage = new MainListAdapter.MainItemEntity();
        vimage.name = "VImage";
        vimage.clasz = ComponentAbility.class.getCanonicalName();
        list.add(vimage);
        MainListAdapter.MainItemEntity nline = new MainListAdapter.MainItemEntity();
        nline.name = "NLine";
        nline.clasz = ComponentAbility.class.getCanonicalName();
        list.add(nline);
        MainListAdapter.MainItemEntity vline = new MainListAdapter.MainItemEntity();
        vline.name = "VLine";
        vline.clasz = ComponentAbility.class.getCanonicalName();
        list.add(vline);
        MainListAdapter.MainItemEntity vgraph = new MainListAdapter.MainItemEntity();
        vgraph.name = "VGraph";
        vgraph.clasz = ComponentAbility.class.getCanonicalName();
        list.add(vgraph);
        MainListAdapter.MainItemEntity progress = new MainListAdapter.MainItemEntity();
        progress.name = "Progress";
        progress.clasz = ComponentAbility.class.getCanonicalName();
        list.add(progress);

        MainListAdapter.MainItemEntity page = new MainListAdapter.MainItemEntity();
        page.name = "Page";
        page.clasz = ComponentAbility.class.getCanonicalName();
        page.data = "component_demo/page_item.json";
        list.add(page);
//        MainListAdapter.MainItemEntity scroller = new MainListAdapter.MainItemEntity();
//        scroller.name = "Scroller";
////          scroller.clasz= ScrollerListActivity.class.getName());
//        list.add(scroller);

        MainListAdapter.MainItemEntity scroller = new MainListAdapter.MainItemEntity();
        scroller.name = "ScrollerVL";
        scroller.clasz = ComponentAbility.class.getCanonicalName();
        scroller.data = "component_demo/slider_item.json";
        scroller.desp = "orientation = V | mode = linear";
        list.add(scroller);
        MainListAdapter.MainItemEntity scroller1 = new MainListAdapter.MainItemEntity();
        scroller1.name = "ScrollerVS";
        scroller1.clasz = ComponentAbility.class.getCanonicalName();
        scroller1.data = "component_demo/slider_item.json";
        scroller1.desp = "orientation = V | mode = staggered";
        list.add(scroller1);
        MainListAdapter.MainItemEntity scroller2 = new MainListAdapter.MainItemEntity();
        scroller2.name = "ScrollerH";
        scroller2.clasz = ComponentAbility.class.getCanonicalName();
        scroller2.data = "component_demo/slider_item.json";
        scroller2.desp = "orientation = H";
        list.add(scroller2);
        notSupportlist.add(scroller1);


        MainListAdapter.MainItemEntity slider = new MainListAdapter.MainItemEntity();
        slider.name = "Slider";
        slider.clasz = ComponentAbility.class.getCanonicalName();
        slider.data = "component_demo/slider_item.json";
        list.add(slider);
//        MainListAdapter.MainItemEntity container = new MainListAdapter.MainItemEntity();
//        container.name = "Container";
//        container.clasz = ComponentAbility.class.getCanonicalName();
//        list.add(container);
        MainListAdapter.MainItemEntity framelayout = new MainListAdapter.MainItemEntity();
        framelayout.name = "FrameLayout";
        framelayout.clasz = ComponentAbility.class.getCanonicalName();
        framelayout.data = "component_demo/frame_style.json";
        list.add(framelayout);
        MainListAdapter.MainItemEntity ratiolayout = new MainListAdapter.MainItemEntity();
        ratiolayout.name = "RatioLayout";
        ratiolayout.clasz = ComponentAbility.class.getCanonicalName();
        list.add(ratiolayout);
        MainListAdapter.MainItemEntity grid = new MainListAdapter.MainItemEntity();
        grid.name = "Grid";
        grid.clasz = ComponentAbility.class.getCanonicalName();
        grid.data = "component_demo/grid_item.json";
        list.add(grid);
        MainListAdapter.MainItemEntity gridlayout = new MainListAdapter.MainItemEntity();
        gridlayout.name = "GridLayout";
        gridlayout.clasz = ComponentAbility.class.getCanonicalName();
        list.add(gridlayout);
        MainListAdapter.MainItemEntity vh2layout = new MainListAdapter.MainItemEntity();
        vh2layout.name = "VH2Layout";
        vh2layout.clasz = ComponentAbility.class.getCanonicalName();
        list.add(vh2layout);
        MainListAdapter.MainItemEntity vhlayout = new MainListAdapter.MainItemEntity();
        vhlayout.name = "VHLayout";
        vhlayout.clasz = ComponentAbility.class.getCanonicalName();
        list.add(vhlayout);
        MainListAdapter.MainItemEntity vh = new MainListAdapter.MainItemEntity();
        vh.name = "VH";
        vh.clasz = ComponentAbility.class.getCanonicalName();
        vh.data = "component_demo/vh_item.json";
        list.add(vh);
        MainListAdapter.MainItemEntity nframelayout = new MainListAdapter.MainItemEntity();
        nframelayout.name = "NFrameLayout";
        nframelayout.clasz = ComponentAbility.class.getCanonicalName();
        list.add(nframelayout);
        MainListAdapter.MainItemEntity ngridlayout = new MainListAdapter.MainItemEntity();
        ngridlayout.name = "NGridLayout";
        ngridlayout.clasz = ComponentAbility.class.getCanonicalName();
        list.add(ngridlayout);
        MainListAdapter.MainItemEntity nratiolayout = new MainListAdapter.MainItemEntity();
        nratiolayout.name = "NRatioLayout";
        nratiolayout.clasz = ComponentAbility.class.getCanonicalName();
        list.add(nratiolayout);
        MainListAdapter.MainItemEntity nvhlayout = new MainListAdapter.MainItemEntity();
        nvhlayout.name = "NVHLayout";
        nvhlayout.clasz = ComponentAbility.class.getCanonicalName();
        list.add(nvhlayout);
        MainListAdapter.MainItemEntity nvh2layout = new MainListAdapter.MainItemEntity();
        nvh2layout.name = "NVH2Layout";
        nvh2layout.clasz = ComponentAbility.class.getCanonicalName();
        list.add(nvh2layout);
//        MainListAdapter.MainItemEntity flexlayout = new MainListAdapter.MainItemEntity();
//        flexlayout.name = "FlexLayout";
//        flexlayout.clasz = ComponentAbility.class.getCanonicalName();
//        list.add(flexlayout);
        MainListAdapter.MainItemEntity totalcontainer = new MainListAdapter.MainItemEntity();
        totalcontainer.name = "TotalContainer";
        totalcontainer.clasz = ComponentAbility.class.getCanonicalName();
        totalcontainer.data = "component_demo/total_container.json";
        list.add(totalcontainer);

        Log.i("ComponentListActivitySlice.initView([]):172   " + list.size());
        MainListAdapter adapter = new MainListAdapter(this, list);
        listContainer.setItemProvider(adapter);

        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {

                if (notSupportlist.contains(list.get(i))) {
                    Toast.show(ComponentListActivitySlice.this, "暂时无法进行实现");
                    return;
                }

                MainListAdapter.MainItemEntity mainItemEntity = (MainListAdapter.MainItemEntity) listContainer.getItemProvider().getItem(i);
                Intent getIntent = new Intent();
                Operation opt = new Intent
                        .OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(mainItemEntity.clasz)
                        .build();
                getIntent.setParam("data", mainItemEntity.data);
                getIntent.setParam("name", mainItemEntity.name);
                getIntent.setOperation(opt);
                getAbility().startAbility(getIntent);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
