package com.tmall.wireless.virtualviewdemo;

import com.tmall.wireless.virtualviewdemo.slice.ScriptListAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ScriptListAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ScriptListAbilitySlice.class.getName());
    }
}
