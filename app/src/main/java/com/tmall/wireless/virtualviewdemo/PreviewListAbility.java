package com.tmall.wireless.virtualviewdemo;

import com.tmall.wireless.virtualviewdemo.slice.PreviewListAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class PreviewListAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PreviewListAbilitySlice.class.getName());
    }
}
