package com.tmall.wireless.virtualviewdemo;

import com.tmall.wireless.virtualviewdemo.slice.TmallComponentListAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class TmallComponentListAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TmallComponentListAbilitySlice.class.getName());
    }
}
