package com.tmall.wireless.virtualviewdemo.slice;

import com.tmall.wireless.vaf.Log;
import com.tmall.wireless.virtualviewdemo.*;
import com.tmall.wireless.virtualviewdemo.provider.MainListAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.TextTool;
import ohos.security.permission.Permission;
import ohos.security.permission.PermissionFlags;

import java.security.Permissions;
import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Log.i("ComponentListActivitySlice.onStart([intent]):21" + this.getBundleName() + "        " + this.getAbility().getClass().getCanonicalName());


        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        List<MainListAdapter.MainItemEntity> list = new ArrayList<>();
        MainListAdapter.MainItemEntity parse = new MainListAdapter.MainItemEntity();
        parse.name = "基础流程演示";
        parse.clasz = ParserDemoAbility.class.getCanonicalName();
        list.add(parse);
        MainListAdapter.MainItemEntity api = new MainListAdapter.MainItemEntity();
        api.name = "内置控件演示";
        api.clasz = ComponentListAbility.class.getCanonicalName();
        list.add(api);
        MainListAdapter.MainItemEntity bizItems = new MainListAdapter.MainItemEntity();
        bizItems.name = "业务场景演示";
        bizItems.clasz = TmallComponentListAbility.class.getCanonicalName();
        list.add(bizItems);
        MainListAdapter.MainItemEntity script = new MainListAdapter.MainItemEntity();
        script.name = "脚本引擎演示(实验中)";
        script.clasz = ScriptListAbility.class.getCanonicalName();
        list.add(script);
        MainListAdapter.MainItemEntity preview = new MainListAdapter.MainItemEntity();
        preview.name = "模板实时预览";
        preview.clasz = PreviewListAbility.class.getCanonicalName();
        list.add(preview);
        MainListAdapter adapter = new MainListAdapter(this, list);
        listContainer.setItemProvider(adapter);
        listContainer.setItemClickedListener((listContainer1, component, i, l) -> {
            MainListAdapter.MainItemEntity mainItemEntity = (MainListAdapter.MainItemEntity) listContainer1.getItemProvider().getItem(i);
            if (TextTool.isNullOrEmpty(mainItemEntity.clasz)) {
                return;
            }
            Intent getIntent = new Intent();
            Operation opt = new Intent
                    .OperationBuilder()
                    .withBundleName(getBundleName())
                    .withAbilityName(mainItemEntity.clasz)
                    .build();
            getIntent.setOperation(opt);
            startAbility(getIntent);
        });

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
