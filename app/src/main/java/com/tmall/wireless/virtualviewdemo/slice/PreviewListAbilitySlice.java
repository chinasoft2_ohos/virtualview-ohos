package com.tmall.wireless.virtualviewdemo.slice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.tmall.wireless.vaf.expr.engine.utils.Toast;
import com.tmall.wireless.virtualviewdemo.ResourceTable;
import com.tmall.wireless.virtualviewdemo.utils.HttpUtil;
import com.tmall.wireless.virtualviewdemo.provider.MainListAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.tmall.wireless.virtualviewdemo.PreviewActivityAbility;
import ohos.agp.components.Text;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.tmall.wireless.virtualviewdemo.slice.PreviewActivityAbilitySlice.FROM_REAL_PREVIEW;
import static com.tmall.wireless.virtualviewdemo.slice.PreviewActivityAbilitySlice.KEY_FROM;

public class PreviewListAbilitySlice extends AbilitySlice {
    private MainListAdapter mAdapter;
    private ListContainer listContainer;
    private Text tips;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_preview_list);
        initView();
    }


    private void initView() {
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        tips = (Text) findComponentById(ResourceTable.Id_tips);
        List<MainListAdapter.MainItemEntity> list = new ArrayList<>();
        mAdapter = new MainListAdapter(this, list);
        listContainer.setItemProvider(mAdapter);
        listContainer.setItemClickedListener((listContainer1, component, i, l) -> {
            MainListAdapter.MainItemEntity mainItemEntity = (MainListAdapter.MainItemEntity) listContainer1.getItemProvider().getItem(i);
            Intent getIntent = new Intent();
            Operation opt = new Intent
                    .OperationBuilder()
                    .withBundleName(getBundleName())
                    .withAbilityName(PreviewActivityAbility.class.getCanonicalName())
                    .build();
            getIntent.setParam("data", mainItemEntity.data);
            getIntent.setParam("name", mainItemEntity.name);
            getIntent.setParam(KEY_FROM, FROM_REAL_PREVIEW);
            getIntent.setOperation(opt);
            getAbility().startAbility(getIntent);
        });

        new Thread(() -> {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(getUrl())
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if (response != null && response.isSuccessful()) {
                    if (response.body() != null) {
                        String string = response.body().string();
                        JSONArray dirs = JSON.parseArray(string);
                        if (dirs != null) {
                            getUITaskDispatcher().syncDispatch(() -> {
                                List<MainListAdapter.MainItemEntity> list1 = new ArrayList<>();
                                for (int i = 0; i < dirs.size(); i++) {
                                    String demoDir = dirs.getString(i);
                                    MainListAdapter.MainItemEntity mainItemEntity = new MainListAdapter.MainItemEntity();
                                    mainItemEntity.name = demoDir;
                                    list1.add(mainItemEntity);
                                }
                                mAdapter.setNewData(list1);
                            });
                        } else {
                            getUITaskDispatcher().syncDispatch(() -> Toast.showShort(PreviewListAbilitySlice.this, "No templates!"));
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                getUITaskDispatcher().syncDispatch(() -> {
                    Toast.showShort(PreviewListAbilitySlice.this, "Server is not running!");
                    listContainer.setVisibility(Component.HIDE);
                    tips.setText("数据加载失败");
                });
            }
        }).start();

    }

    private String getUrl() {
        return HttpUtil.getHostUrl() + ".dir";
    }
}
