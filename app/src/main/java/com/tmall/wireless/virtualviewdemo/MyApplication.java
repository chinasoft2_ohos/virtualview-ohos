package com.tmall.wireless.virtualviewdemo;

import com.libra.virtualview.common.BizCommon;
import com.tmall.wireless.vaf.framework.VafContext;
import com.tmall.wireless.vaf.framework.ViewManager;
import com.tmall.wireless.vaf.virtualview.event.EventManager;
import com.tmall.wireless.vaf.virtualview.view.image.ImageBase;
import com.tmall.wireless.virtualviewdemo.bytes.*;
import com.tmall.wireless.virtualviewdemo.custom.*;
import ch.poole.imageloader.cache.disc.impl.UnlimitedDiskCache;
import ch.poole.imageloader.core.ImageLoader;
import ch.poole.imageloader.core.ImageLoaderConfiguration;
import ch.poole.imageloader.core.assist.FailReason;
import ch.poole.imageloader.core.assist.ImageSize;
import ch.poole.imageloader.core.assist.QueueProcessingType;
import ch.poole.imageloader.core.lintener.ImageLoadingListener;
import ch.poole.imageloader.core.lintener.SimpleImageLoadingListener;
import ohos.aafwk.ability.AbilityPackage;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import java.io.File;

public class MyApplication extends AbilityPackage {

    static MyApplication myApplication;

    public static MyApplication getInstance() {
        if (myApplication == null) {
            myApplication = new MyApplication();
        }
        return myApplication;
    }

    public static void setMyApplication(MyApplication myApplication) {
        MyApplication.myApplication = myApplication;
    }

    private VafContext sVafContext;

    private ViewManager sViewManager;

    @Override
    public void onInitialize() {
        super.onInitialize();
        initialize();
        setMyApplication(this);
    }

    public VafContext getVafContext() {
        return sVafContext;
    }

    public ViewManager getViewManager() {
        return sViewManager;
    }


    private void initialize() {
        if (sVafContext == null) {
            ImageLoader.getInstance().init(getImageLoaderConfiguration(this));
//            VVFeatureConfig.setSliderCompat(true);
            sVafContext = new VafContext(this.getApplicationContext());
            sVafContext.setImageLoaderAdapter(new com.tmall.wireless.vaf.virtualview.Helper.ImageLoader.IImageLoaderAdapter() {

                @Override
                public void bindImage(String uri, final ImageBase imageBase, int reqWidth, int reqHeight) {
                    Component nativeView = imageBase.getNativeView();

                    ImageSize targetImageSize = null;
                    if (reqHeight > 0 || reqWidth > 0) {
                        targetImageSize = new ImageSize(reqWidth, reqHeight);
                    }
                    if (nativeView instanceof Image) {
                        ImageLoader.getInstance().displayImage(uri, (Image) nativeView, targetImageSize);
                    } else {
                        ImageLoader.getInstance().loadImage(uri, targetImageSize, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingComplete(String url, Component component, PixelMap pixelMap) {
                                imageBase.setBitmap(pixelMap);
                            }
                        });
                    }
                }

                @Override
                public void getBitmap(String uri, int reqWidth, int reqHeight, final com.tmall.wireless.vaf.virtualview.Helper.ImageLoader.Listener listener) {
                    ImageSize targetImageSize = null;
                    if (reqHeight > 0 || reqWidth > 0) {
                        targetImageSize = new ImageSize(reqWidth, reqHeight);
                    }

                    ImageLoader.getInstance().loadImage(uri, targetImageSize, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String url, Component component) {

                        }

                        @Override
                        public void onLoadingFailed(String url, Component component, FailReason e) {
                            if (listener != null) {
                                listener.onImageLoadFailed();
                            }
                        }

                        @Override
                        public void onLoadingComplete(String url, Component component, PixelMap pixelMap) {
                            if (listener != null) {
                                listener.onImageLoadSuccess(pixelMap);
                            }
                        }

                        @Override
                        public void onLoadingCancelled(String url, Component component) {

                        }
                    });
                }
            });

            sViewManager = sVafContext.getViewManager();
            sViewManager.init(this.getApplicationContext());
            sViewManager.loadBinBufferSync(NTEXT.getBIN());
            sViewManager.loadBinBufferSync(VTEXT.getBIN());
            sViewManager.loadBinBufferSync(NIMAGE.getBIN());
            sViewManager.loadBinBufferSync(VIMAGE.getBIN());
            sViewManager.loadBinBufferSync(VLINE.getBIN());
            sViewManager.loadBinBufferSync(NLINE.getBIN());
            sViewManager.loadBinBufferSync(PROGRESS.getBIN());
            sViewManager.loadBinBufferSync(VGRAPH.getBIN());
            sViewManager.loadBinBufferSync(PAGE.getBIN());
            sViewManager.loadBinBufferSync(PAGEITEM.getBIN());
            sViewManager.loadBinBufferSync(PAGESCROLLSCRIPT.getBIN());
            sViewManager.loadBinBufferSync(SLIDER.getBIN());
            sViewManager.loadBinBufferSync(SLIDERITEM.getBIN());
            sViewManager.loadBinBufferSync(FRAMELAYOUT.getBIN());
            sViewManager.loadBinBufferSync(RATIOLAYOUT.getBIN());
            sViewManager.loadBinBufferSync(GRIDLAYOUT.getBIN());
            sViewManager.loadBinBufferSync(GRID.getBIN());
            sViewManager.loadBinBufferSync(GRIDITEM.getBIN());
            sViewManager.loadBinBufferSync(VHLAYOUT.getBIN());
            sViewManager.loadBinBufferSync(VH2LAYOUT.getBIN());
            sViewManager.loadBinBufferSync(VH.getBIN());
            sViewManager.loadBinBufferSync(SCROLLERVL.getBIN());
            sViewManager.loadBinBufferSync(SCROLLERVS.getBIN());
            sViewManager.loadBinBufferSync(SCROLLERH.getBIN());
            sViewManager.loadBinBufferSync(TOTALCONTAINER.getBIN());
            sViewManager.loadBinBufferSync(NFRAMELAYOUT.getBIN());
            sViewManager.loadBinBufferSync(NGRIDLAYOUT.getBIN());
            sViewManager.loadBinBufferSync(NRATIOLAYOUT.getBIN());
            sViewManager.loadBinBufferSync(NVHLAYOUT.getBIN());
            sViewManager.loadBinBufferSync(NVH2LAYOUT.getBIN());
            sViewManager.loadBinBufferSync(CLICKSCRIPT.getBIN());
            sViewManager.loadBinBufferSync(TMALLCOMPONENT1.getBIN());
            sViewManager.loadBinBufferSync(TMALLCOMPONENT2.getBIN());
            sViewManager.loadBinBufferSync(TMALLCOMPONENT3.getBIN());
            sViewManager.loadBinBufferSync(TMALLCOMPONENT4.getBIN());
            sViewManager.loadBinBufferSync(TMALLCOMPONENT5.getBIN());
            sViewManager.loadBinBufferSync(TMALLCOMPONENT6.getBIN());
            sViewManager.loadBinBufferSync(TMALLCOMPONENT7.getBIN());
            sViewManager.loadBinBufferSync(TMALLCOMPONENT8.getBIN());
            sViewManager.loadBinBufferSync(PICASSO.getBIN());
            sViewManager.getViewFactory().registerBuilder(BizCommon.TM_TOTAL_CONTAINER, new TotalContainer.Builder());
            sViewManager.getViewFactory().registerBuilder(1014, new PicassoImage.Builder());
            sVafContext.getCompactNativeManager().register("TMTags", TMReminderTagsView.class);
            sVafContext.getEventManager().register(EventManager.TYPE_Click, new ClickProcessorImpl());
            sVafContext.getEventManager().register(EventManager.TYPE_Exposure, new ExposureProcessorImpl());
        }
    }

    private ImageLoaderConfiguration getImageLoaderConfiguration(Context context) {

        int MB = 1038336;
        return new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .threadPoolSize(4)
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .diskCacheSize(256 * MB)
                .diskCache(new UnlimitedDiskCache(new File(
                        context.getCacheDir().toString() + "/uil-images")))
                .memoryCacheSize(8 * MB)
                .writeDebugLogs()
                .build();
    }
}
