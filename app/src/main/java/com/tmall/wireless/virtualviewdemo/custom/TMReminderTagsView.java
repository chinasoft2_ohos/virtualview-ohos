/*
 * MIT License
 *
 * Copyright (c) 2018 Alibaba Group
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.tmall.wireless.virtualviewdemo.custom;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.tmall.wireless.vaf.virtualview.Helper.RtlHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * Created by longerian on 17/4/9.
 *
 * @author longerian
 * @date 2017/04/09
 */

public class TMReminderTagsView extends Component implements IViewInterface, Component.DrawTask, Component.EstimateSizeListener {

    private String[] tags;

    private int[] eachTagWidth;
    private int[] eachTagHeight;

    private Paint bgPaint;

    // private TextPaint textPaint;
    private Paint textPaint;

    private int tagsGap;

    private int tagsWidth;

    private int tagsHeight;

    private int hPadding;

    private Rect tagRect;

    private Paint.FontMetrics textFontMetrics;

    public TMReminderTagsView(Context context) {
        this(context, null);
    }

    public TMReminderTagsView(Context context, AttrSet attrs) {
        this(context, attrs, -1);
    }

    public TMReminderTagsView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (mDensity == -1) {
            initWith(context);
        }
        //textPaint = new TextPaint();
        textPaint = new Paint();
        textPaint.setAntiAlias(true);
        textPaint.setColor(new Color(Color.rgb(255, 59, 68)));
        textPaint.setTextSize(dp2px(10));

        bgPaint = new Paint();
        bgPaint.setColor(new Color(Color.rgb(250, 211, 213)));

        tagsGap = dp2px(7);

        hPadding = dp2px(3);

        tagRect = new Rect();

        textFontMetrics = textPaint.getFontMetrics();
        addDrawTask(this);
        setEstimateSizeListener(this);
    }


    public void setTags(JSONArray tags) {
        if (tags != null) {
            int length = tags.size();
            String[] newTags = new String[length];
            for (int i = 0; i < length; i++) {
                newTags[i] = tags.getString(i);
            }
            setTags(newTags);
        }
    }

    public void setTags(String[] tags) {
        if (tags != null && this.tags != tags) {
            this.tags = tags.clone();
            tagsWidth = 0;
            tagsHeight = (int) (((double)textFontMetrics.descent - (double)textFontMetrics.ascent) + (double) 0.5);
            this.eachTagWidth = new int[tags.length];
            this.eachTagHeight = new int[tags.length];


            for (int i = 0; i < tags.length; i++) {
                Rect bounds = textPaint.getTextBounds(tags[i] != null ? tags[i] : "");
                // int tagWdith = bounds.left + bounds.getWidth();
                int tagWdith = bounds.getWidth() + 2 * hPadding;
//                int tagWdith = (int) (Layout.getDesiredWidth(tags[i] != null ? tags[i] : "", textPaint) + 0.5f) + 2 * hPadding;
                eachTagWidth[i] = tagWdith;
                eachTagHeight[i] = bounds.getHeight();
                tagsWidth += tagWdith;
                if (i < tags.length - 1) {
                    tagsWidth += tagsGap;
                }
            }

            //TODO
//            for (int i = 0; i < tags.length; i++) {
//                //  int tagWdith = (int) (Layout.getDesiredWidth(tags[i] != null ? tags[i] : "", textPaint) + 0.5f) + 2 * hPadding;
//                RichTextLayout richTextLayout = new RichTextLayout();
//                richTextLayout.calculateTextWidth()
//                int tagWdith = (int) (Layout.getDesiredWidth(tags[i] != null ? tags[i] : "", textPaint) + 0.5f) + 2 * hPadding;
//                eachTagWidth[i] = tagWdith;
//                tagsWidth += tagWdith;
//                if (i < tags.length - 1) {
//                    tagsWidth += tagsGap;
//                }
//            }
            // requestLayout();
            postLayout();
            invalidate();
        }
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        boolean rtl = RtlHelper.isRtl();

        if (rtl) {
            int right = getPaddingLeft();
            int top = getPaddingTop();
            for (int i = tags.length - 1; i >= 0; i--) {
                tagRect.right = getWidth() - right;
                tagRect.left = getWidth() - right - eachTagWidth[i];
                tagRect.top = top;
                tagRect.bottom = top + eachTagHeight[i] + getPaddingBottom() + 10;
                canvas.drawRect(tagRect, bgPaint);
                canvas.drawText(textPaint, tags[i], tagRect.left + hPadding,
                        eachTagHeight[i]);
                right += tagsGap + eachTagWidth[i];
            }

        } else {
            int left = getPaddingLeft();
            int top = getPaddingTop();
            for (int i = 0; i < tags.length; i++) {
                tagRect.left = left;
                tagRect.top = top;
                tagRect.right = left + eachTagWidth[i];
                //tagRect.bottom = canvas.getHeight() - getPaddingBottom();
                tagRect.bottom = top + eachTagHeight[i] + getPaddingBottom() + 10;
                canvas.drawRect(tagRect, bgPaint);

//            canvas.drawText(tags[i], left + hPadding,
//                    canvas.getHeight() - getPaddingBottom() - textFontMetrics.descent,
//                    textPaint);
                canvas.drawText(textPaint, tags[i], left + hPadding,
                        eachTagHeight[i]);

                left += tagsGap + eachTagWidth[i];
            }
        }


    }

//    @Override
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//        int left = getPaddingLeft();
//        int top = getPaddingTop();
//        for (int i = 0; i < tags.length; i++) {
//            tagRect.left = left;
//            tagRect.top = top;
//            tagRect.right = left + eachTagWidth[i];
//            tagRect.bottom = canvas.getHeight() - getPaddingBottom();
//            canvas.drawRect(tagRect, bgPaint);
//
//            canvas.drawText(tags[i], left + hPadding,
//                    canvas.getHeight() - getPaddingBottom() - textFontMetrics.descent,
//                    textPaint);
//            left += tagsGap + eachTagWidth[i];
//        }
//    }

    //    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        setMeasuredDimension(tagsWidth + getPaddingRight() + getPaddingLeft(),
//                tagsHeight + getPaddingTop() + getPaddingBottom());
//    }
    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        setEstimatedSize(tagsWidth + getPaddingRight() + getPaddingLeft(),
                tagsHeight + getPaddingTop() + getPaddingBottom());
        return false;
    }

    @Override
    public void onBind(Object jsonObject) {
        if (jsonObject instanceof JSONArray) {
            setTags((JSONArray) jsonObject);
        } else if (jsonObject instanceof String) {
            try {
                setTags(JSON.parseArray((String) jsonObject));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        else if (jsonObject instanceof com.alibaba.fastjson.JSONArray) {
//            setTags((com.alibaba.fastjson.JSONArray) jsonObject);
//        }
    }

    @Override
    public void onUnbind() {

    }

    private static float mDensity = -1f;

    public static void initWith(final Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        mDensity = display.getAttributes().densityPixels;
//        final Resources resources = context.getResources();
//
//        final DisplayMetrics dm = resources.getDisplayMetrics();
//        mDensity = dm.density;
    }

    public static int dp2px(double dpValue) {
        float scaleRatio = mDensity;
        final float scale = scaleRatio < 0 ? 1.0f : scaleRatio;

        int finalValue;
        if (dpValue >= 0) {
            finalValue = (int) (dpValue * scale + 0.5f);
        } else {
            finalValue = -((int) (-dpValue * scale + 0.5f));
        }
        return finalValue;
    }


}
