## 1.4.7

发布1.4.7 release版本

## 0.0.1-SNAPSHOT

virtualview-ohos组件openharmony beta版本

#### api差异

* 基本与原库无差异

#### 已实现功能

* 已实现自定义xml解析与控件的配置展示

#### 未实现功能

* 瀑布流相关页面未实现
