#   Virtualview-ohos



### 项目介绍
- 项目名称：Virtualview-ohos
- 所属系列：openharmony的第三方组件适配移植
- 功能：通过自定义的XML文件及对应的页面展示控件,来组成一套区别于原生系统的控件展示方式.
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 1.4.6

### 效果演示

![VText](./img/VirtualView_show.gif)

详情见DEMO
### 安装教程


1.在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
        maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:virtualView-ohos:1.4.7')
 }
```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

### 使用说明

#### 一.将仓库导入到本地仓库中 
      参照安装教程
#### 二.使用方式
```
VafContext vafContext = new VafContext(mContext.getApplicationContext());
VafContext.loadImageLoader(mContext.getApplicationContext());

ViewManager viewManager = vafContext.getViewManager();
viewManager.init(mContext.getApplicationContext());

//注册控件
sViewManager.getViewFactory().registerBuilder(BizCommon.TM_TOTAL_CONTAINER, new TotalContainer.Builder());
sViewManager.getViewFactory().registerBuilder(1014, new PicassoImage.Builder());
sVafContext.getCompactNativeManager().register("TMTags", TMReminderTagsView.class);

```
```
//加载二进制布局文件
viewManager.loadBinFileSync(TMALLCOMPONENT1_PATH);
viewManager.loadBinFileSync(TMALLCOMPONENT2_PATH);
```
```
//或者直接加载xml布局文件
//解析XML
private byte[] compile(String type, String name, int version) {
        ViewCompilerApi viewCompiler = new ViewCompilerApi();
        viewCompiler.setConfigLoader(new AssetConfigLoader());
        InputStream fis = null;
        try {
            String filePath = String.format("assets/app/resources/rawfile/%s", name);
            fis = this.getClass().getClassLoader().getResourceAsStream(filePath);

            byte[] result = viewCompiler.compile(fis, type, version);

            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

//解析XML
 private JSONObject getJSONDataFromAsset(String name) {
        try {
            String filePath = String.format("assets/app/resources/rawfile/%s", name);
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(filePath);
            BufferedReader inputStreamReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String str;
            while ((str = inputStreamReader.readLine()) != null) {
                sb.append(str);
            }
            inputStreamReader.close();
            return JSON.parseObject(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

//获取XML解析内容并解析
 private void paseJosn() {
        byte[] binResult = compile(TYPE, PLAY, 1);
        if (binResult != null) {
            MyApplication.getInstance().getViewManager().loadBinBufferSync(binResult);
            Component container = MyApplication.getInstance().getVafContext().getContainerService().getContainer(TYPE, true);
            IContainer iContainer = (IContainer) container;
            JSONObject json = getJSONDataFromAsset(PLAY_DATA);
            if (json != null) {
                iContainer.getVirtualView().setVData(json);
            }
            Layout.Params p = iContainer.getVirtualView().getComLayoutParams();
            DirectionalLayout.LayoutConfig marginLayoutParams = new DirectionalLayout.LayoutConfig(p.mLayoutWidth, p.mLayoutHeight);
            marginLayoutParams.setMarginLeft(p.mLayoutMarginLeft);
            marginLayoutParams.setMarginTop(p.mLayoutMarginTop);
            marginLayoutParams.setMarginRight(p.mLayoutMarginRight);
            marginLayoutParams.setMarginBottom(p.mLayoutMarginBottom);

            mLinearLayout.removeAllComponents();
            mLinearLayout.addComponent(container, marginLayoutParams);

        } else {
            Toast.showShort(this, "编译出错，检查日志");
        }
    }
```
```
//注册事件监听
sVafContext.getEventManager().register(EventManager.TYPE_Click, new ClickProcessorImpl());
sVafContext.getEventManager().register(EventManager.TYPE_Exposure, new ExposureProcessorImpl());
```
实际使用见DEMO演示

### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

### 版本迭代
- 1.4.7
- 0.0.1-SNAPSHOT

### 版权和许可信息

VirtualView is available under the MIT license.